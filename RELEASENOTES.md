# RELEASE NOTES: *jemma*, Application to teach young children the basics of programming..

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 3.0.5**:
  - Updated build system components.


**Version 3.0.4**:
  - Updated build system.


**Version 3.0.3**:
  - Reordered RELEASENOTES file.
  

**Version 3.0.2**:
  - !!!Forget this version!!!


**Version 3.0.1**:
  - Fixed bug in editor.


**Version 3.0.0**:
  - Re-architectured java project due to the redesign of the editor.
  - Now, different languages are no more directly accessible but to be chosen at launching time via the dedicated parameter.


**Version 2.1.1**:
  - Updated tool tips.


**Version 2.1.0**:
  - Added the display of row and column numbers in the grid.
  - Modified a little bit the application-embedded documentation.
  - Improved java sources.
  - Fixed bug: expression evaluation in debug mode.
  - Fixed bug: runnnig symbol not cleared when debug is stopped.


**Version 2.0.0**:
  - Moved to dark interface.
  - Wrapped lines in Console.
  - Added the display of the drawing area dimensions, updated in real time.
  - Improved *color* and *direction* pages in JEMMA usage (*click me*).
  - Added a on-demand To tracing aid grid.
  - Integrated a first draft of the new language "geometry"  not yet accessible to users.
  - Sources restructuration to ease introduction of the new language.
  - Added arguments "-v|--verb" (default) and "-g|--geometry" to select programming language, and "-u|-unique" to only use the selected or predefined language.


**Version 1.0.0**:
  - First version.
