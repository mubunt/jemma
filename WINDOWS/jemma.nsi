;${APPNAME} NSIS MScript
;Written by Michel Rizzo

;!pragma warning error all

;--------------------------------
!include "MUI2.nsh"
;--------------------------------
!define APPNAME "jemma"
!define COMPANYNAME "JEMMA"
!define KITDIR "JEMMA_WINDOWS"
!define MUI_ABORTWARNING
!define MUI_LANGDLL_ALLLANGUAGES
!define MUI_HEADERIMAGE_BITMAP ".\${APPNAME}HEADER.bmp"
!define MUI_WELCOMEFINISHPAGE_BITMAP ".\${APPNAME}WELCOME.bmp"
;Language Selection Dialog Settings
!define MUI_LANGDLL_REGISTRY_ROOT "HKCU" 
!define MUI_LANGDLL_REGISTRY_KEY "Software\${APPNAME}" 
!define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"
!define APPNAMEICON "${APPNAME}.ico"
!define APPNAMEICONG "${APPNAME}G.ico"
;--------------------------------
;General
Unicode true
Name "${APPNAME}"
OutFile "${APPNAME}R3Setup.exe"
InstallDir "$LOCALAPPDATA\${APPNAME}"
InstallDirRegKey HKCU "Software\${APPNAME}" ""
;--------------------------------
;Pages
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE ".\LICENSE.txt"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
;------------------------------
!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH
;--------------------------------
;Languages
!insertmacro MUI_LANGUAGE "French"
!insertmacro MUI_LANGUAGE "English"
;--------------------------------
;Installer Sections
Section "Installation" SecInstall
  SetOutPath "$INSTDIR"
  File "${KITDIR}\${APPNAME}.exe"
  File "${KITDIR}\${APPNAMEICON}"
  File "${KITDIR}\${APPNAMEICONG}"
  SetOutPath "$INSTDIR\${APPNAME}_bin"
  File /r "${KITDIR}\${APPNAME}_bin\*"
  ;Store installation folder
  WriteRegStr HKCU "Software\${APPNAME}" "" $INSTDIR
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
SectionEnd
Section "Stat Menu Shortcut" StartShortcut
  SetShellVarContext current
  createDirectory "$SMPROGRAMS\${COMPANYNAME}"
  createShortCut "$SMPROGRAMS\${COMPANYNAME}\${APPNAME}.lnk" "$INSTDIR\${APPNAME}.exe" "" "$INSTDIR\${APPNAMEICON}"
  createShortCut "$SMPROGRAMS\${COMPANYNAME}\Uninstall ${APPNAME}.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\${APPNAMEICON}"
SectionEnd
Section "Desktop Shortcut" SecShortcut
    SetShellVarContext current
    CreateShortCut "$DESKTOP\${APPNAME}.lnk" "$INSTDIR\${APPNAME}.exe" "--french --verb" "$INSTDIR\${APPNAMEICON}" 0 SW_SHOWMINIMIZED
    CreateShortCut "$DESKTOP\${APPNAME}G.lnk" "$INSTDIR\${APPNAME}.exe" "--french --geometry" "$INSTDIR\${APPNAMEICONG}" 0 SW_SHOWMINIMIZED
SectionEnd
;--------------------------------
Function .onInit
  !insertmacro MUI_LANGDLL_DISPLAY
FunctionEnd
;--------------------------------
Section "Uninstall"
  Delete "$SMPROGRAMS\${APPNAME}\${APPNAME}.lnk"
  Delete "$SMPROGRAMS\${APPNAME}\Uninstall ${APPNAME}.lnk"
  rmDir "$SMPROGRAMS\${APPNAME}"
  Delete "$INSTDIR\${APPNAME}.exe"
  Delete "$INSTDIR\${APPNAMEICON}"
  Delete "$INSTDIR\${APPNAMEICONG}"
  RMDir /r $INSTDIR\${APPNAME}_bin
  Delete "$INSTDIR\Uninstall.exe"
  RMDir "$INSTDIR"
  Delete "$DESKTOP\${APPNAME}.lnk"
  Delete "$DESKTOP\${APPNAME}G.lnk"
  DeleteRegKey /ifempty HKCU "Software\${APPNAME}"
SectionEnd
;--------------------------------
Function un.onInit
  #Verify the uninstaller - last chance to back out
  MessageBox MB_OKCANCEL "Suppression permananente de ${APPNAME}?" IDOK next
    Abort
  next:
  !insertmacro MUI_UNGETLANGUAGE
FunctionEnd
;--------------------------------
