 # *jemma*, Application to teach young children the basics of programming..

<img align="left" src="./images/jemma.png">

**jemma** is an application to support the learning of programming to young children (6 to 10 years). It introduces the notion of *instruction*, *expression*, named memory cell (*variables* in other words) and *command*. Its primitive look and feel is intended to prevent the children from the excesses of the graphics and the mouse (repeated clicks, copy-paste inopportune, etc.).

To interest the child, the goal of any **jemma** program  is to draw a picture.

**jemma** supports 2 programming languages and the interface allows you to choose which child wants to use. The first, **VERB**, is the simplest. It is based on the use of verbs (MOVE FORWARD, GO BACK, SKIP, TURN, DO n TIMES, ASSIGN, etc.) and allows you to draw straight lines. The second, **GEOMETRY**, is a little more complex even if it uses a subset of **VERB** instructions. It is dedicated to tracing geometric figures (lines, rectangles, triangles, ovals, etc.) and implements the concept of coordinates. These programming language are chosen at launch time. The default is **VERB**.

**jemma** is available in 2 languages: english and french. The choice is made when the application is launched and the default language is ..... French.

![Example](./README_images/jemma-01.png  "Example 1 - VERB programming language (french version)")


![Example](./README_images/jemma-02.png  "Example 2 - VERB programming language (french version)")


![Example](./README_images/jemma-03.png  "Example 3 - GEOMETRY programming language (french version)")


![Example](./README_images/jemma-04.png  "Example 4 - GEOMETRY programming language (french version)")

All documentation is embedded and accessible via the **Click me** button. This documentation covers how to enter a program and how to check / execute / debug it and all aspects of the supported language (instructions, supported colors and other items), The user is also helped with tips displayant dedicated information to particularize an instruction.

![Example](./README_images/jemma-05.png  "Example 5 - GEOMETRY programming language (english version)")

## LICENSE
**jemma** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ jemma -V
jemma	Version: 2.1.0 - Build: 40 - Date: May, 2020
$ jemma -h
usage: jemma - JAVA application to teach young children the basics of programming
 -E,--english    dialogue in english language
 -F,--french     dialogue in french language (default)
 -g,--geometry   programming language is GEOMETRY
 -h,--help       print this message and exit
 -V,--version    print the version information and exit
 -v,--verb       programming language is VERB (default)

$ jemma -F -g

```
## STRUCTURE OF THE APPLICATION
This section walks you through **jemma**'s structure. Once you understand this structure, you will easily find your way around in **jemma**'s code base.

``` bash
$ yaTree
./                                                               # Application level
├── LINUX/                                                       # Scripts and data files for LINUX platform management
│   └── JEMMA.desktop                                            # 
├── README_images/                                               # Images for documentation
│   ├── EM.png                                                   # 
│   ├── jemma-01.png                                             # 
│   ├── jemma-02.png                                             # 
│   ├── jemma-03.png                                             # 
│   ├── jemma-04.png                                             # 
│   └── jemma-05.png                                             # 
├── WINDOWS/                                                     # Scripts and data files for WINDOWS platform management
│   ├── LICENSE.txt                                              # 
│   ├── cmdmp3.exe                                               # 
│   ├── jemma.bmp                                                # 
│   ├── jemma.ico                                                # 
│   ├── jemma.nsi                                                # 
│   ├── jemmaG.ico                                               # 
│   ├── jemmaHEADER.bmp                                          # 
│   ├── jemmaWELCOME.bmp                                         # 
│   └── makeKit                                                  # 
├── examples/                                                    # JEMMA examples directory
│   ├── Makefile                                                 # Makefile
│   ├── p1.jemma                                                 # Program JEMMA (VERB programming language)
│   ├── p1.jemmaG                                                # Program JEMMA (GEOMETRY programming language)
│   ├── p2.jemma                                                 # Program JEMMA (VERB programming language)
│   ├── p2.jemmaG                                                # Program JEMMA (GEOMETRY programming language)
│   ├── p3.jemma                                                 # Program JEMMA (VERB programming language)
│   ├── p3.jemmaG                                                # Program JEMMA (GEOMETRY programming language)
│   ├── p4.jemma                                                 # Program JEMMA (VERB programming language)
│   ├── p4.jemmaG                                                # Program JEMMA (GEOMETRY programming language)
│   ├── p5.jemma                                                 # Program JEMMA (VERB programming language)
│   ├── p6.jemma                                                 # Program JEMMA (VERB programming language)
│   ├── p7.jemma                                                 # Program JEMMA (VERB programming language)
│   ├── p8.jemma                                                 # Program JEMMA (VERB programming language)
│   └── p9.jemma                                                 # Program JEMMA (VERB programming language)
├── images/                                                      # Images directory
│   ├── Makefile                                                 # Makefile
│   ├── clickme.png                                              # JEMMA icon for document access (verb language)
│   ├── clickmeG.png                                             # JEMMA icon for document access (geometry language)
│   ├── home.png                                                 # Drawing stating point icon
│   ├── jemma.png                                                # JEMMA icon
│   └── jemmaG.png                                               # 
├── lib/                                                         # Third-party jar files directory
│   ├── Makefile                                                 # Makefile
│   ├── commons-cli-1.4.jar                                      # COMMONS CLI jar file
│   ├── org.eclipse.core.commands_3.9.700.v20191217-1850.jar     # CORE COMMANDS jar file
│   ├── org.eclipse.equinox.common_3.11.0.v20200206-0817.jar     # EQUINOX COMMON jar file
│   ├── org.eclipse.jface_3.18.0.v20191122-2109.jar              # ECLIPSE JFACE jar file
│   ├── org.eclipse.nebula.widgets.pshelf_1.1.0.201912241810.jar # ECLIPSE NEBULA (PSHELF) jar file
│   ├── swt-linux-415.jar                                        # ECLIPSE SWT jar file for Linux
│   └── swt-windows-415.jar                                      # ECLIPSE SWT jar file for Windows
├── sounds/                                                      # Sounds directory
│   ├── Car-crash-sound-effect.mp3                               # Sound for out-of-limit error
│   ├── Makefile                                                 # Makefile
│   └── beep.mp3                                                 # Sound for beep
├── src_c/                                                       # C Source directory
│   ├── Makefile                                                 # Makefile
│   └── jemma.c                                                  # JEMMA driver source
├── src_java/                                                    # JAVA Source directory
│   ├── jemma/                                                   # IntelliJ JEMMA project structure
│   │   ├── src/                                                 # 
│   │   │   ├── jemma.java                                       # 
│   │   │   ├── jemmaBuildInfo.java                              # 
│   │   │   ├── jemmaCommand.java                                # 
│   │   │   ├── jemmaConsole.java                                # 
│   │   │   ├── jemmaDocumentation.java                          # 
│   │   │   ├── jemmaEditor.java                                 # 
│   │   │   ├── jemmaEditorOverlay.java                          # 
│   │   │   ├── jemmaErrorDialog.java                            # 
│   │   │   ├── jemmaExecution.java                              # 
│   │   │   ├── jemmaExecution_Geometry.java                     # 
│   │   │   ├── jemmaExecution_Verb.java                         # 
│   │   │   ├── jemmaExpression.java                             # 
│   │   │   ├── jemmaLanguage.java                               # 
│   │   │   ├── jemmaLanguage_Geometry.java                      # 
│   │   │   ├── jemmaLanguage_Verb.java                          # 
│   │   │   ├── jemmaSounds.java                                 # 
│   │   │   └── jemmaUI.java                                     # 
│   │   └── jemma.iml                                            # 
│   └── Makefile                                                 # Makefile
├── COPYING.md                                                   # GNU General Public License markdown file
├── LICENSE.md                                                   # License markdown file
├── Makefile                                                     # Makefile
├── README.md                                                    # ReadMe markdown file
├── RELEASENOTES.md                                              # Release Notes markdown file
└── VERSION                                                      # Version identification text file

11 directories, 74 files
$ 
```
## HOW TO BUILD THIS APPLICATION
### For Linux
```Shell
$ cd jemma
$ make clean all
```
### For Windows
On Ubuntu:
```Shell
$ cd jemma
$ make wclean wall
```

## HOW TO INSTALL AND USE THIS APPLICATION ON LOCAL MACHINE
```Shell
$ cd jemma
$ make release
    # Executable (generated with -O2 option), jar files, and associated files are installed in $BIN_DIR directory (defined at environment level). We consider that $BIN_DIR is a part of the PATH.
```

## HOW TO BUILD AN SOFTWARE INSTALLATION PACKAGE FOR WINDOWS
On Ubuntu:
```Shell
$ cd jemma
$ make wrelease
$ cd WINDOWS
$ ./makeKit
```
Then,
  - Transfer on Windows 10 computer, in a specific folder, files **jemmaWIN_Tools.tar.gz** and **jemmaWIN_Tree.tar.gz**.
  - Extract them in the folder.
  - Compile using NSIS (Nullsoft Scriptable Install System) v3.05 (or highre), the script file jemma.nsi.
  - **jemma[Rx]Setup.exe** is the JEMMA installation kit.

## SOFTWARE REQUIREMENTS
- For usage and dor development: JAVA
- Image (*jemma.png*) comes from http://registerforbetting.com.
- On Windows, the command Line MP3 Player comes from https://lawlessguy.wordpress.com/2015/06/27/update-to-a-command-line-mp3-player-for-windows/.
- On Windows, Nullsoft Scriptable Install System software v3.05
- On Ubuntu, the command Line MP3 Player is *mpg123* (from *mpg321* package).
- Developped and tested on XUBUNTU 20.04, GCC v9.3.0, OPENJDK v1.8.0_252

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***