//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
public class jemmaLanguage_Verb {
	static String ITEM_DIRECTION;
	static String USAGE_DIRECTION;
	static String KEYWORD_LEFT;
	static String KEYWORD_RIGHT;
	static String KEYWORD_TIMES;
	static String KEYWORD_STEPS;
	static String KEYWORD_TO;

	static void init_language( String language ) {
		jemma.suffixLanguage 				= "jemma";

		String KEYWORD_MOVEFORWARD		= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "MOVEFORWARD" : "AVANCER";
		String KEYWORD_GOBACKWARD		= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "GOBACK" : "RECULER";
		String KEYWORD_SKIP				= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "SKIP" : "SAUTER";
		String KEYWORD_TURN				= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "TURN" : "TOURNER";
		String KEYWORD_DO 				= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "DO" : "EXECUTER";
		String KEYWORD_ENDDO			= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "END" : "FIN";
		String KEYWORD_ASSIGN 			= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "ASSIGN" : "METTRE";
		String KEYWORD_COLOR 			= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "COLOR" : "COLORER";
		String KEYWORD_USE 				= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "USE" : "UTILISER";
		KEYWORD_LEFT					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "LEFT" : "GAUCHE";
		KEYWORD_RIGHT					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "RIGHT" : "DROITE";
		KEYWORD_TIMES 					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "TIMES" : "FOIS";
		KEYWORD_STEPS					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "STEPS" : "PAS";
		KEYWORD_TO						= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "TO" : "DANS";

		String UNIT_EXPRESSION			= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "expression" : "expression") + "]";
		String UNIT_VARIABLE			= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "variable" : "variable") + "]";
		String UNIT_COLOR				= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "color" : "couleur") + "]";
		String UNIT_DIRECTION			= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "direction" : "direction") + "]";

		String SUITE_INSTRUCTION_MOVEFORWARD	= UNIT_EXPRESSION + " " + KEYWORD_STEPS;
		String SUITE_INSTRUCTION_GOBACKWARD		= UNIT_EXPRESSION + " " + KEYWORD_STEPS;
		String SUITE_INSTRUCTION_SKIP			= UNIT_EXPRESSION + " " + KEYWORD_STEPS;
		String SUITE_INSTRUCTION_TURN			= UNIT_DIRECTION;
		String SUITE_INSTRUCTION_DO 			= UNIT_EXPRESSION + " " + KEYWORD_TIMES;
		String SUITE_INSTRUCTION_ENDDO 			= "";
		String SUITE_INSTRUCTION_ASSIGN 		= UNIT_EXPRESSION + " " + KEYWORD_TO + " " + UNIT_VARIABLE;
		String SUITE_INSTRUCTION_COLOR 			= UNIT_COLOR;
		String SUITE_INSTRUCTION_USE	 		= UNIT_EXPRESSION;

		ITEM_DIRECTION					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "Directions" : "Directions";

		jemmaLanguage.USAGE_INSTRUCTION	= "\n" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
				"Program instructions can be:\n\n" +
						"\t\uFFFA" + KEYWORD_MOVEFORWARD	+ " " + SUITE_INSTRUCTION_MOVEFORWARD	+ "\uFFFB\n\t\t\tDraw a line of n units, n being the result of the expression.\n" +
						"\t\uFFFA" + KEYWORD_GOBACKWARD		+ " " + SUITE_INSTRUCTION_GOBACKWARD	+ "\uFFFB\n\t\t\tDraw a line too, but in reverse.\n" +
						"\t\uFFFA" + KEYWORD_SKIP			+ " " + SUITE_INSTRUCTION_SKIP			+ "\uFFFB\n\t\t\tSkip n units (no visible line).\n" +
						"\t\uFFFA" + KEYWORD_TURN			+ " " + SUITE_INSTRUCTION_TURN			+ "\uFFFB\n\t\t\tTurn right or left at a right angle.\n" +
						"\t\uFFFA" + KEYWORD_DO 			+ " " + SUITE_INSTRUCTION_DO			+ "\uFFFB\n\t\t\ttRepeat the following instructions until "+ KEYWORD_ENDDO + " n times, n being the result of the expression.\n" +
						"\t\uFFFA" + KEYWORD_ENDDO 			+ " " + SUITE_INSTRUCTION_ENDDO			+ "\uFFFB\n\t\t\tThis instruction delimits the end of the sequence to repeat.\n" +
						"\t\uFFFA" + KEYWORD_ASSIGN 		+ " " + SUITE_INSTRUCTION_ASSIGN		+ "\uFFFB\n\t\t\tPut the result of the expression in the named variable.\n" +
						"\t\uFFFA" + KEYWORD_COLOR 			+ " " + SUITE_INSTRUCTION_COLOR			+ "\uFFFB\n\t\t\tDefine line color.\n" +
						"\t\uFFFA" + KEYWORD_USE 			+ " " + SUITE_INSTRUCTION_USE			+ "\uFFFB\n\t\t\tDefine the thickness of the line. The higher the result of the expression, the thicker the line.\n" :
				"Les instructions d'un programme peuvent être :\n\n" +
						"\t\uFFFA" + KEYWORD_MOVEFORWARD 	+ " " + SUITE_INSTRUCTION_MOVEFORWARD	+ "\uFFFB\n\t\t\tTracer un trait de n unités, n étant le résultat de l'expression.\n" +
						"\t\uFFFA" + KEYWORD_GOBACKWARD 	+ " " + SUITE_INSTRUCTION_GOBACKWARD	+ "\uFFFB\n\t\t\tTracer un trait aussi, mais dans le sens inverse.\n" +
						"\t\uFFFA" + KEYWORD_SKIP			+ " " + SUITE_INSTRUCTION_SKIP			+ "\uFFFB\n\t\t\tSauter n unités (pas de trait visible).\n" +
						"\t\uFFFA" + KEYWORD_TURN 			+ " " + SUITE_INSTRUCTION_TURN			+ "\uFFFB\n\t\t\tTourner à droite ou à gauche en angle droit.\n" +
						"\t\uFFFA" + KEYWORD_DO 			+ " " + SUITE_INSTRUCTION_DO			+ "\uFFFB\n\t\t\tRépéter les instructions qui suivent jusqu'à \uFFFA" + KEYWORD_ENDDO + "\uFFFB n fois, n étant le résultat de l'expression.\n" +
						"\t\uFFFA" + KEYWORD_ENDDO 			+ " " + SUITE_INSTRUCTION_ENDDO			+ "\uFFFB\n\t\t\tCette instruction délimite la fin de la séquence à répéter.\n" +
						"\t\uFFFA" + KEYWORD_ASSIGN 		+ " " + SUITE_INSTRUCTION_ASSIGN		+ "\uFFFB\n\t\t\tMettre le résultat de l'expression dans la variable nommée.\n" +
						"\t\uFFFA" + KEYWORD_COLOR 			+ " " + SUITE_INSTRUCTION_COLOR			+ "\uFFFB\n\t\t\tDéfinir la couleur du trait.\n" +
						"\t\uFFFA" + KEYWORD_USE 			+ " " + SUITE_INSTRUCTION_USE			+ "\uFFFB\n\t\t\tDéfinir l'épaisseur du trait. Plus le résultat de l'expression est élevé, plus le trait est épais.\n");


		jemmaLanguage.instructionTab = new jemmaLanguage.instructionClass[] {
				new jemmaLanguage.instructionClass(KEYWORD_MOVEFORWARD,	SUITE_INSTRUCTION_MOVEFORWARD,	jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_GOBACKWARD,	SUITE_INSTRUCTION_GOBACKWARD,	jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_SKIP,		SUITE_INSTRUCTION_SKIP,			jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_TURN,		SUITE_INSTRUCTION_TURN,			jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_DO,			SUITE_INSTRUCTION_DO,			jemmaLanguage.instructionClass.eIndentation.RIGHT),
				new jemmaLanguage.instructionClass(KEYWORD_ENDDO,		SUITE_INSTRUCTION_ENDDO,		jemmaLanguage.instructionClass.eIndentation.LEFT),
				new jemmaLanguage.instructionClass(KEYWORD_ASSIGN,		SUITE_INSTRUCTION_ASSIGN,		jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_COLOR,		SUITE_INSTRUCTION_COLOR,		jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_USE,			SUITE_INSTRUCTION_USE,			jemmaLanguage.instructionClass.eIndentation.NONE),
		};

		jemmaLanguage.unitTIPS = new jemmaLanguage.tips[] {
				new jemmaLanguage.tips(UNIT_COLOR,		(jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Available colours are: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE and GRAY.\n" :
						"Les couleurs disponibles sont : NOIR, ROUGE, VERT, JAUNE, BLEU, MAGENTA, CYAN, BLANC et GRIS.")),
				new jemmaLanguage.tips(UNIT_EXPRESSION,	(jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"An expression is a combination of following operations, may be between parenthesis. An operand is a variable or a number.\n\n" +
								"Addition:			operand + operand\n" +
								"Substraction:		operand - operand\n" +
								"Multiplication:		operand * operand\n" +
								"Division:			operand / operand\n\n" +
								"Examples: N + 3 or (N + 3) * 2" :
						"Une expression est une combinaison des opérations suivantes, peut-être entre parenthèses. Un opérande est une variable ou un nombre.\n\n" +
								"Addition:			opérande + opérande\n" +
								"Soustraction:		opérande - opérande\n" +
								"Multiplication:		opérande * opérande\n" +
								"Division:			opérande / opérande\n\n" +
								"Exemples : N + 3 ou (N + 3) * 2")),
				new jemmaLanguage.tips(UNIT_VARIABLE,	(jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Variables are symbols that associate a name (identifier) with a value. The name is unique but the value may change over time. For the name of a variable:\n" +
								"	- there can only be lowercase, uppercase and numbers;\n" +
								"	- the name of the variable must start with a letter;\n" +
								"	- spaces are prohibited. Instead, you can use the underscore character _;\n" +
								"	- you are not allowed to use accents.\n" :
						"Les variables sont des symboles qui associent un nom (l'identifiant) à une valeur. Le nom est unique mais la valeur peut changer au cours du temps. Pour le nom d'une variable :\n" +
								"	- il ne peut y avoir que des minuscules, majuscules et des chiffres;\n" +
								"	- le nom de la variable doit commencer par une lettre;\n" +
								"	- les espaces sont interdits. À la place, on peut utiliser le caractère « underscore » _;\n" +
								"	- tu n'as pas le droit d'utiliser des accents.\n")),
				new jemmaLanguage.tips(UNIT_DIRECTION, 	(jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Directions are:\n\n" :
						"Les directions sont :\n\n") +
						"	- " + KEYWORD_LEFT + "\n" +
						"	- " + KEYWORD_RIGHT + "\n")
		};

		jemmaLanguage.USAGE_FIRST		= "\n" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
				"To enter a program, click with the mouse in the green window on the left, under the icon.\n\nThen start typing the instruction you want to use. At any time, you can hit the \uFFFATAB\uFFFB key; \uFFFAJEMMA\uFFFB will then complete the instruction that you have started. You will have to adapt the instruction to your needs by selecting with the mouse the fields to define, for example \uFFFA[expression]\uFFFB, \uFFFA[variable]\uFFFB, \uFFFA[color]\uFFFB or \uFFFA[direction]\uFFFB, to replace them with your values (for example \uFFFA30\uFFFB, \uFFFAA\uFFFB, \uFFFAred\uFFFB or \uFFFAright\uFFFB. To do so, select it by double-clicking with the left mouse button. If you leave the mouse for a few parts of seconds on a field to be defined, help will be displayed on the screen.\n\nThe \uFFFATAB\uFFFB key which allows you to complete the instruction you have started entering, also allows you to display all of the \uFFFAJEMMA\uFFFB commands if you use it at the start of the line. If several commands start with the same letter or letters, then the \uFFFATAB\uFFFB key is also used to display them.\n\nWhen you wish, you can click on the \uFFFACHECK\uFFFB button; \uFFFAJEMMA\uFFFB will then check that what you have written is correct. The errors will be displayed in the console. The console is the green window, bottom left.\n\nAlways when you wish, you can click on the \uFFFAEXECUTE\uFFFB button. You will then see the result of your creation in the window on the right. It may not be the expected result .... because you may have been wrong!!!\n\nGood game..." :
				"Pour entrer un programme, clique avec la souris dans la fenêtre verte à gauche, sous l'icône.\n\nPuis commence à taper l'instruction que tu veux utiliser. A tout moment, tu peux taper sur la touche \uFFFATAB\uFFFB; \uFFFAJEMMA\uFFFB complètera alors l'instruction que tu as commencé. Il te restera à adapter l'instruction à tes besoins en sélectionnant avec la souris les champs à définir, par example \uFFFA[expression]\uFFFB, \uFFFA[variable]\uFFFB, \uFFFA[couleur]\uFFFB ou \uFFFA[direction]\uFFFB, pour les remplacer par tes valeurs (par exemple \uFFFA30\uFFFB, \uFFFAA\uFFFB, \uFFFArouge\uFFFB ou \uFFFAdroite\uFFFB. Pour cela, sélectionne-le, en double-cliquant avec la touche gauche de la souris. Si tu laisses la souris quelques pouièmes de secondes sur un champs à définir, une aide sera alors afficher à l'écran.\n\nLa touche \uFFFATAB\uFFFB qui te permet de compléter l'instruction que tu as commencé à entrer, te permet aussi à afficher l'ensemble des commandes de \uFFFAJEMMA\uFFFB si tu l'utilises en début de ligne. Si plusieurs commandes commencent pat la ou les mêmes lettres, alors la touche \uFFFATAB\uFFFB permet aussi de les afficher.\n\nQuand tu le souhaites, tu peux cliquer sur le bouton \uFFFAVERIFIER\uFFFB; \uFFFAJEMMA\uFFFB contrôlera alors que ce que tu as écrit est correct. Les erreurs seront affichées dans la console. La console est la fenêtre verte, en bas à gauche.\n\nToujours quand tu le souhaiteras, tu pourras cliquer sur le bouton \uFFFAEXECUTER\uFFFB. Tu verras alors le résultat de ta création dans la fénêtre de droite. Ce ne sera peut être pas le résultat attendu.... car tu t'es peu-être trompé !!!\n\nBon jeu...");

		USAGE_DIRECTION					= "\n" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
				"Directions are:\n\n" +
						"\t\uFFFA" + KEYWORD_LEFT + "\uFFFB\n" +
						"\t\uFFFA" + KEYWORD_RIGHT + "\uFFFB\n\n" +
						"For the clumsy :-) :-):\n\n" +
						"\t\t\t\t\t\t\t\tThe LEFT is there\n" +
						"If I go to this direction\t\uFFFA----->\uFFFB\n" +
						"\t\t\t\t\t\t\t\tThe RIGHT is there\n" +
						"\n" +
						"If I go to this direction\t\uFFFA|\uFFFB\n" +
						"\t\t\t\t\t\t\t\t\t\t\uFFFA|\uFFFB\n" +
						"\t\t\t\t\t\t\t\t\t\t\uFFFA|\uFFFB\n" +
						"\tThe RIGHT is there\t\uFFFAv\uFFFB\tThe LEFT is there\n" +
						"\n" +
						"\n" +
						"\tThe RIGHT is there\n" +
						"\t\t\t\t\t\t\uFFFA<-----\uFFFB\tIf I go to this direction\n" +
						"\tThe LEFT is there\n" +
						"\n" +
						"\n" +
						"\tThe LEFT is there\t\uFFFA^\uFFFB\tThe RIGHT is there\n" +
						"\t\t\t\t\t\t\t\t\t\t\uFFFA|\uFFFB\n" +
						"\t\t\t\t\t\t\t\t\t\t\uFFFA|\uFFFB\n" +
						"If I go to this direction\t\uFFFA|\uFFFB\n" :
				"Les directions sont :\n\n" +
						"\t\uFFFA" + KEYWORD_LEFT + "\uFFFB\n" +
						"\t\uFFFA" + KEYWORD_RIGHT + "\uFFFB\n\n" +
						"Pour les maladroit(e)s :-):-) :\n\n:" +
						"\t\t\t\t\t\t\t\tLa GAUCHE est là\n" +
						"Si je vais dans ce sens\t\uFFFA----->\uFFFB\n" +
						"\t\t\t\t\t\t\t\tLa DROITE est là\n" +
						"\n" +
						"Si je vais dans ce sens\t\uFFFA|\uFFFB\n" +
						"\t\t\t\t\t\t\t\t\t\t\t\uFFFA|\uFFFB\n" +
						"\t\t\t\t\t\t\t\t\t\t\t\uFFFA|\uFFFB\n" +
						"\tLa DROITE est là\t\t\tv\tLa GAUCHE est là\n" +
						"\n" +
						"\n" +
						"\tLa DROITE est là\n" +
						"\t\t\t\t\t\t\uFFFA<-----\uFFFB\tSi je vais dans ce sens\n" +
						"\tLa GAUCHE est là\n" +
						"\n" +
						"\n" +
						"\tLa GAUCHE est là\t\t\uFFFA^\uFFFB\tLa DROITE est là\n" +
						"\t\t\t\t\t\t\t\t\t\t\t\uFFFA|\uFFFB\n" +
						"\t\t\t\t\t\t\t\t\t\t\t\uFFFA|\uFFFB\n" +
						"Si je vais dans ce sens\t\uFFFA|\uFFFB\n");
	}
}
