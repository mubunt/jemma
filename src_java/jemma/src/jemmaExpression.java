//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Code adapted from https://www.geeksforgeeks.org/expression-evaluation/.
// Thanks to authors.
//------------------------------------------------------------------------------
import java.util.Stack;

public class jemmaExpression {
	static class evalExpression {
		boolean valid = true;
		String expression = "";
		String error = "";
		int result = 0;
		private String initial_expression;

		void init( String s) {
			initial_expression = expression = s.replaceAll(" ", "");
		}
		void error( String s ) {
			error = s;
			valid = false;
			expression = initial_expression;
			result = 0;
		}
	}
	private static evalExpression expr = null;

	public static evalExpression evaluate(String expression) {
		expr = new evalExpression();
		expr.init(expression);

		char[] tokens = expression.toCharArray();
		// Stack for numbers: 'values'
		Stack<Integer> values = new Stack<>();
		// Stack for Operators: 'ops'
		Stack<Character> ops = new Stack<>();
		boolean needAnOperator = false;
		for (int i = 0; i < tokens.length; i++) {
			// Current token is a whitespace, skip it
			if (tokens[i] == ' ' || tokens[i] == '\t') continue;
			// Current token is the "running symble" => equivalent to end
			if (tokens[i] == jemmaUI.runningSymbol.charAt(0)) break;
			// Current token is an opening brace, push it to 'ops'
			if (tokens[i] == '(') {
				ops.push(tokens[i]);
				continue;
			}
			// Closing brace encountered, solve entire brace
			if (tokens[i] == ')') {
				try {
					while (ops.peek() != '(') {
						try {
							if (! applyOp(ops.pop(), values.pop(), values.pop())) return expr;
							values.push(expr.result);
						} catch ( Exception e) {
							expr.error(jemmaLanguage.ERROR_EXPRESSION);
							return expr;
						}
					}
					ops.pop();
				} catch ( Exception e) {
					expr.error(jemmaLanguage.ERROR_EXPRESSION);
					return expr;
				}
				continue;
			}

			// Current token is a number, push it to stack for numbers
			if (tokens[i] >= '0' && tokens[i] <= '9') {
				if (needAnOperator) {
					expr.error(jemmaLanguage.ERROR_EXPRESSION);
					return expr;
				}
				StringBuilder sbuf = new StringBuilder();
				while (i < tokens.length && tokens[i] >= '0' && tokens[i] <= '9')
					sbuf.append(tokens[i++]);
				values.push(Integer.parseInt(sbuf.toString()));
				--i;
				needAnOperator = true;
				continue;
			}
			// Current token is a variable, get the associated value and push it to stack for numbers
			if (Character.isLetter(tokens[i])) {
				if (needAnOperator) {
					expr.error(jemmaLanguage.ERROR_EXPRESSION);
					return expr;
				}
				StringBuilder sbuf = new StringBuilder();
				while (i < tokens.length && (Character.isLetter(tokens[i]) || Character.isDigit(tokens[i]) || tokens[i] == '_'))
					sbuf.append(tokens[i++]);
				jemmaExecution.symbol m = jemmaExecution.symbolsTable_get(jemmaExecution.symbolsTable, sbuf.toString());
				if (m == null) {
					jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.WARNING_VAR_UNDEFINED, jemmaConsole.msgType.ERROR);
					jemmaExecution.symbolsTable_create(jemmaExecution.symbolsTable, sbuf.toString());
					values.push(0);
				} else {
					values.push(m.value);
				}
				--i;
				needAnOperator = true;
				continue;
			}

			// Current token is an operator.
			if (tokens[i] == '+' || tokens[i] == '-' || tokens[i] == '*' || tokens[i] == '/') {
				needAnOperator = false;
				// While top of 'ops' has same or greater precedence to current
				// token, which is an operator. Apply operator on top of 'ops'
				// to top two elements in values stack
				try {
					while (!ops.empty() && hasPrecedence(tokens[i], ops.peek())) {
						if (! applyOp(ops.pop(), values.pop(), values.pop())) return expr;
						values.push(expr.result);
					}
					// Push current token to 'ops'.
					ops.push(tokens[i]);
				} catch ( Exception e) {
					expr.error(jemmaLanguage.ERROR_EXPRESSION);
					return expr;
				}
				continue;
			}
			expr.error(jemmaLanguage.ERROR_EXPRESSION);
			return expr;
		}
		// Entire expression has been parsed at this point, apply remaining
		// ops to remaining values
		while (!ops.empty()) {
			try {
				if (! applyOp(ops.pop(), values.pop(), values.pop())) return expr;
				values.push(expr.result);
			} catch ( Exception e) {
				expr.error(jemmaLanguage.ERROR_EXPRESSION);
				return expr;
			}
		}
		// Top of 'values' contains result, return it
		try {
			expr.result= values.pop();
		} catch ( Exception e) {
			expr.error(jemmaLanguage.ERROR_EXPRESSION);
		}
		return expr;
	}

	// Returns true if 'op2' has higher or same precedence as 'op1',
	// otherwise returns false.
	public static boolean hasPrecedence(char op1, char op2) {
		if (op2 == '(' || op2 == ')')
			return false;
		return (op1 != '*' && op1 != '/') || (op2 != '+' && op2 != '-');
	}

	// A utility method to apply an operator 'op' on operands 'a' and 'b'.
	public static boolean applyOp(char op, int b, int a) {
		switch (op) {
			case '+':
				expr.result = a + b;
				break;
			case '-':
				expr.result = a - b;
				break;
			case '*':
				expr.result = a * b;
				break;
			case '/':
				if (b == 0)
					expr.error(jemmaLanguage.ERROR_DIVIDE);
				else
					expr.result = a / b;
				break;
			default:
				expr.error(jemmaLanguage.ERROR_EXPRESSION);
		}
		return expr.valid;
	}
}
