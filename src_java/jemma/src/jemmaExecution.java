//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Color;

import java.util.List;
import java.util.Stack;

public class jemmaExecution {
	static class symbol {
		String name;
		int value;
		void init(String name) {
			this.name = name;
			this.value = 0;
		}
	}
	static class oneInstruction_result {
		boolean correctness;
		int lineno;
	}
	static class instdo {
		int lineno;
		int counter;
		void setDo(int lineno, int counter) {
			this.lineno = lineno;
			this.counter = counter;
		}
		boolean loopOrNot() {
			--this.counter;
			return this.counter > 0;
		}
	}

	static int lineno;
	static String programLine;
	static int nestedLoops;
	static int offset;
	static StringBuilder analyzed_program;
	static List<symbol> symbolsTable;
	static Stack<instdo> loop;

	static final Color[] systemColors = {
			jemmaUI.black,
			jemmaUI.blue,
			jemmaUI.cyan,
			jemmaUI.gray,
			jemmaUI.darkgray,
			jemmaUI.green,
			jemmaUI.magenta,
			jemmaUI.red,
			jemmaUI.white,
			jemmaUI.yellow
	};
	//--------------------------------------------------------------------------
	static symbol symbolsTable_get(List<symbol> symbolsTable, String name ) {
		for (symbol m : symbolsTable) {
			if (m.name.equals(name)) return m;
		}
		return null;
	}
	static void symbolsTable_put( List<symbol> symbolsTable, String name, int value ) {
		for (symbol m : symbolsTable) {
			if (m.name.equals(name)) {
				m.value = value;
				break;
			}
		}
	}
	static void symbolsTable_create(List<symbol> symbolsTable, String name) {
		symbol m = symbolsTable_get(symbolsTable, name);
		if (m == null) {
			m = new symbol();
			m.init(name);
			symbolsTable.add(m);
		} else
			m.value = 0;
	}
	//--------------------------------------------------------------------------
	static boolean analyzer(boolean executionRequested) {
		if (jemma.ProgrammingLanguage == jemmaLanguage.eLanguage.VERB)
			return jemmaExecution_Verb.analyzer(executionRequested);
		return jemmaExecution_Geometry.analyzer(executionRequested);
	}
	//--------------------------------------------------------------------------
	static void stepByStepExecutionStart( ) {
		if (jemma.ProgrammingLanguage == jemmaLanguage.eLanguage.VERB)
			jemmaExecution_Verb.stepByStepExecutionStart();
		else
			jemmaExecution_Geometry.stepByStepExecutionStart();
	}
	static boolean stepByStepExecutionNext( ) {
		if (jemma.ProgrammingLanguage == jemmaLanguage.eLanguage.VERB)
			return jemmaExecution_Verb.stepByStepExecutionNext();
		return jemmaExecution_Geometry.stepByStepExecutionNext();
	}

	//--------------------------------------------------------------------------
	static char takeAChar( ) {
		analyzeWhitespaces();
		return jemmaExecution.programLine.charAt(offset++);
	}
	static String takeAWord( ) {
		analyzeWhitespaces();
		int index;
		for (index = offset; index < jemmaExecution.programLine.length(); index++) {
			if (programLine.charAt(index) == ' ' || programLine.charAt(index) == '\t') break;
		}
		String s = programLine.substring(offset, index);
		offset = offset + s.length();
		return s;
	}
	static String takeAString( ) {
		StringBuilder s = new StringBuilder();
		if (offset >= programLine.length() || programLine.charAt(offset) != '"') return "";
		offset = offset + 1;
		while (programLine.charAt(offset) != '"' && offset < programLine.length()) {
			s.append(programLine.charAt(offset));
			offset++;
		}
		if (offset >= programLine.length()) return "";
		return s.toString();
	}
	static boolean notEndOfLine( ) {
		analyzeWhitespaces();
		if (programLine.substring(offset).equals(jemmaUI.runningSymbol)) return false;
		return offset < programLine.length();
	}
	static String addNestedLoopPrefix( int n ) {
		final String nestedLoopPrefix = "    ";
		if (n > 0) {
			return new String(new char[n]).replace("\0", nestedLoopPrefix);
		}
		return "";
	}
	static void analyzeWhitespaces( ) {
		while (offset < programLine.length() && (programLine.charAt(offset) == ' ' || programLine.charAt(offset) == '\t'))
			++offset;
	}
	static int analyzeInstruction( ) {
		int i;
		offset = 0;
		String s = takeAWord().toUpperCase();
		for (i = 0; i < jemmaLanguage.instructionTab.length; i++)
			if (s.equals(jemmaLanguage.instructionTab[i].keywordInstruction)) break;
		if (i == jemmaLanguage.instructionTab.length) return -1;
		return i;
	}
	static String analyzeColor( String s ) {
		int i;
		for (i = 0; i < jemmaLanguage.colorsTab.length; i++) if (s.equals(jemmaLanguage.colorsTab[i])) break;
		if (i == jemmaLanguage.colorsTab.length) return "";
		return s;
	}
	static String analyzeVariable( List<symbol> symbolsTable ) {
		String s = takeAWord();
		if ( ! Character.isLetter(s.charAt(0))) return "";
		for (int i = 1; i < s.length(); i++)
			if ( ! Character.isLetter(s.charAt(i)) && ! Character.isDigit(s.charAt(i)) && s.charAt(i) != '_') return "";
		symbolsTable_create(symbolsTable, s);
		return s;
	}
	static jemmaExpression.evalExpression analyzeExpression(String stop ) {
		analyzeWhitespaces();
		int n;
		if (stop.length() == 0)
			n = programLine.length();
		else
			n = programLine.indexOf(stop, offset);
		if (n == -1) n = programLine.toUpperCase().indexOf(stop, offset);
		if (n == -1) {
			jemmaExpression.evalExpression e = new jemmaExpression.evalExpression();
			e.init("");
			e.error(jemmaLanguage.ERROR_INCRCT_INST);
			return e;
		}
		String s = programLine.substring(offset, n);
		offset = n;
		return jemmaExpression.evaluate(s);
	}
	//--------------------------------------------------------------------------
	static Color getSystemColor(String color ) {
		for (int i = 0; i < jemmaLanguage.colorsTab.length; i++)
			if (color.equals(jemmaLanguage.colorsTab[i])) return systemColors[i];
		return systemColors[0];
	}
	//--------------------------------------------------------------------------
	static void clearDrawing( ) {
		if (jemma.ProgrammingLanguage == jemmaLanguage.eLanguage.VERB)
			jemmaExecution_Verb.clearDrawing();
		else
			jemmaExecution_Geometry.clearDrawing();
	}
	//--------------------------------------------------------------------------
	static void Drawing( PaintEvent event) {
		if (jemma.ProgrammingLanguage == jemmaLanguage.eLanguage.VERB)
			jemmaExecution_Verb.Drawing(event);
		else
			jemmaExecution_Geometry.Drawing(event);
	}
	//--------------------------------------------------------------------------
	static boolean displayError( int lineno, String message ) {
		if (lineno < 0)
			jemmaConsole.writeConsole(jemma.Prompt,  message, jemmaConsole.msgType.ERROR);
		else
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_LINE + (lineno + 1) + message, jemmaConsole.msgType.ERROR);
		return false;
	}
}
