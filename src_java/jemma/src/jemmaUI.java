//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

public class jemmaUI {
	static Display display 							= null;
	static final String EOL							= "\r\n";
	private static final String iconFile 			= "images/clickme.png";
	private static final String iconFileG 			= "images/clickmeG.png";
	private static final String homeFile 			= "images/home.png";
	private static final String sCopyright			= "JEMMA - Copyright 2020, Michel Rizzo";

	private static final int editor_width 			= 500;
	private static final int editor_height			= 400;
	private static final int console_height			= 100;
	private static final int execution_width		= 500;
	private static final int execAreaSize_width		= 80;
	private static boolean modifiedProgram 			= false;
	private static boolean notToBeConsideredAsModification = false;

	private static final String arrowDown			= "\u2b07";
	private static final String arrowRight			= "\u27a1";
	static final String runningSymbol 				= "\u25C0";
	private static final String runningString 		= "\t" + runningSymbol;

	static final Font fontGrid = new Font(Display.getCurrent(), "Arial", 5, SWT.BOLD);
	static final Font fontTitleInfo = new Font(Display.getCurrent(), "Arial", 9, SWT.BOLD);
	static final Font fontInfo = new Font(Display.getCurrent(), "Arial", 9, SWT.NORMAL);
	private static final Font fontProgramming = new Font(Display.getCurrent(), "Courier", 10, SWT.BOLD);
	private static final Font fontConsole = new Font(Display.getCurrent(), "Arial", 9, SWT.NORMAL);
	private static final Font fontCopy = new Font(Display.getCurrent(), "Arial", 8, SWT.ITALIC);
	private static final Font fontLanguage = new Font(Display.getCurrent(), "Arial", 9, SWT.BOLD);
	private static final Font fontInformation = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
	private static final Font fontTitle = new Font(Display.getDefault(), "Arial", 20, SWT.BOLD);
	private static final Font fontButton = new Font(Display.getDefault(), "Arial", 8, SWT.BOLD);

	static final Color black = Display.getDefault().getSystemColor(SWT.COLOR_BLACK);
	static final Color blue = Display.getDefault().getSystemColor(SWT.COLOR_BLUE);
	static final Color cyan = Display.getDefault().getSystemColor(SWT.COLOR_CYAN);
	static final Color gray = Display.getDefault().getSystemColor(SWT.COLOR_GRAY);
	static final Color darkgray = Display.getDefault().getSystemColor(SWT.COLOR_DARK_GRAY);
	static final Color green = Display.getDefault().getSystemColor(SWT.COLOR_GREEN);
	static final Color magenta = Display.getDefault().getSystemColor(SWT.COLOR_MAGENTA);
	static final Color red = Display.getDefault().getSystemColor(SWT.COLOR_RED);
	static final Color white = Display.getDefault().getSystemColor(SWT.COLOR_WHITE);
	static final Color yellow = Display.getDefault().getSystemColor(SWT.COLOR_YELLOW);
	//static final Color lightgray = Display.getDefault().getSystemColor(SWT.COLOR_LIGHT_GRAY);
	//static final Color orange = Display.getDefault().getSystemColor(SWT.COLOR_ORANGE);
	//static final Color pink = Display.getDefault().getSystemColor(SWT.COLOR_PINK);

	static final Color mySlightlyLighterDarkGray = new Color(Display.getDefault(), 80, 80, 80);
	static final Color myFloralWhite = new Color(Display.getDefault(), 255, 250, 240);

	static final Color executionarea_background = white;
	private static final Color myGold = new Color(Display.getDefault(), 255, 215, 0);
	private static final Color myDarkGray = new Color(Display.getDefault(), 64, 64, 64);
	private static final Color myDarkGreen = new Color(Display.getDefault(), 0, 40, 0);
	private static final Color information_background = white;
	private static final Color information_foreground = black;
	private static final Color informationActive_background = myGold;
	private static final Color informationActive_foreground = black;
	private static final Color title_foreground = white;
	private static final Color button_background = mySlightlyLighterDarkGray;
	private static final Color button_foreground = black;
	private static final Color buttonActive_background = gray;
	private static final Color copy_foreground = white;
	private static final Color shell_background = myDarkGray;

	private static Button saveButton, saveAsButton, clearButton, executeButton, checkButton;
	private static Button debugButton, debugNextButton, debugStopButton;

	static Canvas ExecutionArea = null;
	static StyledText editor;

	enum context { MODIFICATION, CLEANED, LOADED, SAVED, EXECUTED, DEBUGGED, NEXTDEBUGGED, STOPDEBUGGED, CHECKED }
	//--------------------------------------------------------------------------
	static void screen() {
		display = getDisplay();
		final Shell shell = getShell(display);

		Composite topLevel = new Composite(shell, SWT.FILL);
		topLevel.setLayout(new GridLayout(5, false));
		topLevel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		initClickMe(topLevel);
		initTitle(topLevel);
		displaySeparation(topLevel);
		initEditor(topLevel);
		initCommands(topLevel);
		initExecutionArea(topLevel);
		initBottom(topLevel);

		shell.open();
		shell.pack();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		display.dispose();
		jemma.Exit(0);
	}
	//--------------------------------------------------------------------------
	private static Display getDisplay() {
		Display d = Display.getCurrent();
		if (d == null) d = Display.getDefault();
		return d;
	}
	//--------------------------------------------------------------------------
	private static Shell getShell(Display display) {
		Shell shell = new Shell(display);
		shell.setLayout(new GridLayout(1, true));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setCursor(display.getSystemCursor(SWT.CURSOR_ARROW));
		shell.setBackground(shell_background);
		shell.setText(jemma.jemmaName + " - " +
				" Version " + jemmaBuildInfo.getVersion() +
				" Build #" + jemmaBuildInfo.getBuildNumber() +
				" dated " + jemmaBuildInfo.getBuildDate());
		shell.addListener(SWT.Close, (Event e) -> { e.doit = false; jemma.Exit(0); });
		// Center the shell on the primary monitor
		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle sh = shell.getBounds();
		int x = bounds.x + (bounds.width - sh.width) / 2;
		int y = bounds.y + (bounds.height - sh.height) / 2;
		shell.setLocation(x, y);
		return shell;
	}
	//--------------------------------------------------------------------------
	private static void displaySeparation( Composite parent ) {
		Label separator = new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL);
		final GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 5;
		separator.setLayoutData(gd);
	}
	//--------------------------------------------------------------------------
	private static void initClickMe( Composite parent ) {
		Image image;
		if (jemma.ProgrammingLanguage == jemmaLanguage.eLanguage.VERB)
			image = new Image(display, jemma.installRootDir + iconFile);
		else
			image = new Image(display, jemma.installRootDir + iconFileG);
		Button infoButton = new Button(parent, SWT.PUSH);
		final GridData gd = new GridData(SWT.BEGINNING, SWT.END, false, false);
		gd.horizontalSpan = 1;
		infoButton.setLayoutData(gd);
		infoButton.setImage(image);
		infoButton.setFont(fontInformation);
		infoButton.setBackground(information_background);
		infoButton.setForeground(information_foreground);
		infoButton.addListener(SWT.Selection, e -> {
			if (e.type == SWT.Selection) jemmaDocumentation.displayInformation();
		});
		Listener mouseHover = e -> {
			if (e.type == SWT.MouseEnter) {
				infoButton.setBackground(informationActive_background);
				infoButton.setForeground(informationActive_foreground);
			} else {
				infoButton.setBackground(information_background);
				infoButton.setForeground(information_foreground);
			}
		};
		infoButton.addListener(SWT.MouseEnter, mouseHover);
		infoButton.addListener(SWT.MouseExit, mouseHover);
		infoButton.setEnabled(true);
	}
	//--------------------------------------------------------------------------
	private static void initTitle( Composite parent ) {
		Label title = new Label(parent, SWT.WRAP);
		final GridData gd = new GridData(SWT.BEGINNING, SWT.CENTER, true, true);
		gd.horizontalSpan = 3;
		title.setLayoutData(gd);
		title.setText(jemmaLanguage.ui_title);
		title.setFont(fontTitle);
		title.setForeground(title_foreground);
	}
	//--------------------------------------------------------------------------
	private static void initEditor( Composite parent ) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayout(new GridLayout(1, true));
		final GridData gd = new GridData(SWT.FILL, SWT.FILL, false, false);
		gd.horizontalSpan = 2;
		area.setLayoutData(gd);

		editor = jemmaEditor.getEditor(area, jemmaLanguage.instructionTab);
		final GridData gd1 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd1.heightHint = editor_height;
		gd1.widthHint = editor_width;
		editor.setLayoutData(gd1);
		editor.addVerifyListener(event -> {
			if (! notToBeConsideredAsModification)
				possibleCommandAfter(context.MODIFICATION);
		});

		StyledText console = jemmaConsole.getConsole(area);
		final GridData gd2 = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd2.heightHint = console_height;
		console.setLayoutData(gd2);

		jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_HELLO, jemmaConsole.msgType.INFORMATION);
	}
	//--------------------------------------------------------------------------
	private static void initCommands( Composite parent ) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayout(new GridLayout(1, true));
		final GridData gd = new GridData(GridData.FILL, GridData.FILL, false, false);
		gd.horizontalSpan = 1;
		area.setLayoutData(gd);

		checkButton = initButton(area, jemmaLanguage.COMMAND_CHECK, false, new command_check());
		executeButton = initButton(area, jemmaLanguage.COMMAND_RUN, false, new command_run());
		debugButton = initButton(area, jemmaLanguage.COMMAND_DEBUG, false, new command_debug());
		initDebugButton(area);
		clearButton = initButton(area, jemmaLanguage.COMMAND_CLEAR, false, new command_clear());
		initButton(area, jemmaLanguage.COMMAND_GRID, true, new command_grid());
		saveButton = initButton(area, jemmaLanguage.COMMAND_SAVE, false, new command_save());
		saveAsButton = initButton(area, jemmaLanguage.COMMAND_SAVEAS.replace(" ", "\n"), false, new command_saveas());
		initButton(area, jemmaLanguage.COMMAND_LOAD, true, new command_load());
		initButton(area, jemmaLanguage.COMMAND_EXIT, true, new command_exit());
	}
	private static Button initButton( Composite parent, String label, boolean enable, SelectionListener proc ) {
		Button b = new Button(parent, SWT.PUSH);
		b.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		b.setText(label);
		b.setFont(fontButton);
		b.setBackground(button_background);
		b.setForeground(button_foreground);
		b.addSelectionListener(proc);
		Listener mouseHover = e -> {
			if (e.type == SWT.MouseEnter) b.setBackground(buttonActive_background);
			else b.setBackground(button_background);
		};
		b.addListener(SWT.MouseEnter, mouseHover);
		b.addListener(SWT.MouseExit, mouseHover);
		b.setEnabled(enable);
		return b;
	}
	private static void initDebugButton( Composite parent ) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayout(new GridLayout(2, true));
		debugNextButton = initButton(area, jemmaLanguage.COMMAND_DEBUGINST, false, new command_debugNext());
		Label label1 = new Label (area, SWT.NONE);
		label1.setText ("");
		debugStopButton =  initButton(area, jemmaLanguage.COMMAND_DEBUGSTOP, false, new command_debugStop());
		Label label2 = new Label (area, SWT.NONE);
		label2.setText ("");
	}
	//--------------------------------------------------------------------------
	static void initExecutionArea( Composite parent ) {
		ExecutionArea = new Canvas(parent, SWT.NONE);
		ExecutionArea.setBackground(executionarea_background);
		if (jemma.ProgrammingLanguage == jemmaLanguage.eLanguage.VERB)
			ExecutionArea.addListener(SWT.Paint, homeListen);
		ExecutionArea.addPaintListener(jemmaExecution::Drawing);
		final GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd.horizontalSpan = 2;
		gd.widthHint = execution_width;
		ExecutionArea.setLayoutData(gd);
	}
	static Listener homeListen = event -> event.gc.drawImage(new Image(display, jemma.installRootDir + homeFile), 0,0);
	//--------------------------------------------------------------------------
	private static void initBottom( Composite parent ) {
		Label copyright = new Label(parent, SWT.NONE);
		final GridData gd1 = new GridData(SWT.BEGINNING, SWT.TOP, true, false);
		gd1.horizontalSpan = 4;
		copyright.setLayoutData(gd1);
		copyright.setText(sCopyright);
		copyright.setFont(fontCopy);
		copyright.setForeground(copy_foreground);
		copyright.setBackground(shell_background);

		Label execAreaSize = new Label(parent, SWT.NONE);
		final GridData gd2 = new GridData(SWT.END, SWT.TOP, false, false);
		gd2.horizontalSpan = 1;
		gd2.widthHint = execAreaSize_width;
		execAreaSize.setLayoutData(gd2);
		execAreaSize.setFont(fontCopy);
		execAreaSize.setForeground(copy_foreground);
		execAreaSize.setBackground(shell_background);
		ExecutionArea.addListener (SWT.Resize, e -> {
			Rectangle rect = ExecutionArea.getClientArea();
			execAreaSize.setText(arrowDown + " " + rect.height + "   " + arrowRight + " " + rect.width);
		});
	}
	//==========================================================================
	static void highLightLine( int lineno ) {
		int lineoffset = editor.getOffsetAtLine(lineno);
		lineoffset = lineoffset + (editor.getLine(lineno).length());
		editor.setSelection(lineoffset);
		notToBeConsideredAsModification = true;
		editor.insert(runningString);
		notToBeConsideredAsModification = false;
	}
	static void unhighLightLine( int lineno ) {
		int lineoffset = editor.getOffsetAtLine(lineno);
		String s = editor.getLine(lineno);
		int n = s.length();
		s = s.replace(runningString, "");
		notToBeConsideredAsModification = true;
		editor.replaceTextRange(lineoffset, n, s);
		notToBeConsideredAsModification = false;
	}
	//--------------------------------------------------------------------------
	static boolean isProgramModified( ) {
		return modifiedProgram;
	}
	//--------------------------------------------------------------------------
	static void possibleCommandAfter( context act ) {
		if (checkButton == null) return;
		switch (act) {
			case MODIFICATION:
				modifiedProgram = true;
				checkButton.setEnabled(true);
				executeButton.setEnabled(true);
				debugButton.setEnabled(true);
				saveButton.setEnabled(true);
				saveAsButton.setEnabled(true);
				debugNextButton.setEnabled(false);
				debugStopButton.setEnabled(false);
				break;
			case LOADED:
			case SAVED:
				modifiedProgram = false;
				checkButton.setEnabled(true);
				executeButton.setEnabled(true);
				debugButton.setEnabled(true);
				debugNextButton.setEnabled(false);
				debugStopButton.setEnabled(false);
				break;
			case CLEANED:
				clearButton.setEnabled(false);
				break;
			case EXECUTED:
				clearButton.setEnabled(true);
				debugButton.setEnabled(true);
				break;
			case DEBUGGED:
				checkButton.setEnabled(false);
				executeButton.setEnabled(false);
				debugButton.setEnabled(false);
				saveButton.setEnabled(false);
				saveAsButton.setEnabled(false);
				clearButton.setEnabled(false);
				debugNextButton.setEnabled(true);
				debugStopButton.setEnabled(true);
				break;
			case CHECKED:
			case NEXTDEBUGGED:
				break;
			case STOPDEBUGGED:
				checkButton.setEnabled(true);
				executeButton.setEnabled(true);
				debugButton.setEnabled(true);
				clearButton.setEnabled(true);
				saveButton.setEnabled(true);
				saveAsButton.setEnabled(true);
				debugNextButton.setEnabled(false);
				debugStopButton.setEnabled(false);
				break;
		}
	}
	//--------------------------------------------------------------------------
	static void exit() {
		fontTitle.dispose();
		fontButton.dispose();
		fontProgramming.dispose();
		fontConsole.dispose();
		fontInfo.dispose();
		fontTitleInfo.dispose();
		fontCopy.dispose();
		fontInformation.dispose();
		fontLanguage.dispose();
		fontGrid.dispose();

		myGold.dispose();
		myDarkGray.dispose();
		myFloralWhite.dispose();
		mySlightlyLighterDarkGray.dispose();
		myDarkGreen.dispose();
	}
}