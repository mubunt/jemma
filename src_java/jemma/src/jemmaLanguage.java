//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
public class jemmaLanguage {
	static final String LANGUAGE_FRENCH			= "french";
	static final String LANGUAGE_ENGLISH		= "english";

	enum eLanguage { VERB, GEOMETRY }

	static class instructionClass {
		enum eIndentation { NONE, RIGHT, LEFT }
		String keywordInstruction;
		String suiteInstruction;
		eIndentation indentInstruction;
		public instructionClass(String key, String suite, eIndentation type) {
			this.keywordInstruction = key;
			this.suiteInstruction = suite;
			this.indentInstruction = type;
		}
	}
	static class tips {
		String unit;
		String tips;
		public tips(String unit, String tips) {
			this.unit = unit;
			this.tips = tips;
		}
	}
	//--------------------------------------------------------------------------
	static String[] colorsTab;
	static instructionClass[] instructionTab;
	static tips[] unitTIPS;
	static String USAGE_FIRST;
	static String USAGE_INSTRUCTION;
	//--------------------------------------------------------------------------
	static String ui_title;
	static String COMMAND_CHECK;
	static String COMMAND_RUN;
	static String COMMAND_DEBUG;
	static String COMMAND_DEBUGINST;
	static String COMMAND_DEBUGSTOP;
	static String COMMAND_CLEAR;
	static String COMMAND_SAVE;
	static String COMMAND_SAVEAS;
	static String COMMAND_LOAD;
	static String COMMAND_EXIT;
	static String COMMAND_GRID;

	static String KEYWORD_BLACK;
	static String KEYWORD_RED;
	static String KEYWORD_GREEN;
	static String KEYWORD_YELLOW;
	static String KEYWORD_BLUE;
	static String KEYWORD_MAGENTA;
	static String KEYWORD_CYAN;
	static String KEYWORD_WHITE;
	static String KEYWORD_GRAY;
	static String KEYWORD_DARKGRAY;

	static String ITEM_FIRST;
	static String ITEM_INSTRUCTION;
	static String ITEM_COLOR;
	static String ITEM_EXPRESSION;
	static String ITEM_VARIABLE;
	static String ITEM_COMMAND;
	static String ITEM_EXAMPLE;

	static String UNIT_COLOR;
	static String UNIT_USAGE;
	static String UNIT_LANGUAGE_VERB;
	static String UNIT_LANGUAGE_GEOMETRY;

	static String USAGE_COMMAND;
	static String USAGE_EXPRESSION;
	static String USAGE_VARIABLE;
	static String USAGE_EXAMPLE;

	static String MSG_HELLO;
	static String MSG_CONFIRM_EXIT;
	static String MSG_CONFIRM_LOAD;
	static String MSG_CONFIRM_EMPTY;
	static String MSG_EXISTING_FILE;
	static String MSG_CLEAN_DONE;
	static String MSG_LOAD_DONE;
	static String MSG_SAVE_DONE;
	static String MSG_START_CHECKING;
	static String MSG_PRG_CORRECT;
	static String MSG_PRG_EMPTY;
	static String MSG_LINE;
	static String MSG_START_RUNNING;
	static String MSG_PRG_EXECUTED;
	static String MSG_PRG_ONGOING;
	static String MSG_PRG_STOPDEBUG;
	static String MSG_VERB_LANGUAGE;
	static String MSG_GEOMETRY_LANGUAGE;
	static String MSG_CONFIRM;

	static String TITLE_LOAD;
	static String TITLE_SAVE;
	static String TITLE_CONFIRM;

	static String INTERNALERROR;

	static String ERROR_READ_FILE;
	static String ERROR_WRITE_FILE;
	static String ERROR_UNKNOWN_INST;
	static String ERROR_END_OF_INST;
	static String ERROR_EXPRESSION;
	static String ERROR_DIRECTION;
	static String ERROR_COLOR;
	static String ERROR_VARIABLE;
	static String ERROR_DIVIDE;
	static String ERROR_NESTED_LOOPS;
	static String ERROR_INCRCT_INST;
	static String ERROR_LOOP_NULL;
	static String ERROR_EXEC_STOP;
	static String ERROR_SOUND;
	static String ERROR_WALL;

	static String WARNING_VAR_UNDEFINED;
	//==========================================================================
	static void init_language( String language ) {
		ui_title				= LANGUAGE_ENGLISH.equals(language) ?
				"JEMMA ... To learn to program while having fun." :
				"JEMMA ... Pour apprendre à programmer en s'amusant.";
		COMMAND_CHECK 			= LANGUAGE_ENGLISH.equals(language) ? "CHECK" : "VERIFIER";
		COMMAND_RUN 			= LANGUAGE_ENGLISH.equals(language) ? "RUN" : "EXECUTER";
		COMMAND_DEBUG			= LANGUAGE_ENGLISH.equals(language) ? "DEBUG" : "DEBOGUER";
		COMMAND_DEBUGINST		= LANGUAGE_ENGLISH.equals(language) ? "Inst." : "Inst.";
		COMMAND_DEBUGSTOP		= LANGUAGE_ENGLISH.equals(language) ? "Stop" : "Arrêt";
		COMMAND_CLEAR 			= LANGUAGE_ENGLISH.equals(language) ? "CLEAR" : "EFFACER";
		COMMAND_SAVE 			= LANGUAGE_ENGLISH.equals(language) ? "SAVE" : "ENREGISTRER";
		COMMAND_SAVEAS 			= LANGUAGE_ENGLISH.equals(language) ? "SAVE AS" : "ENREGISTRER SOUS";
		COMMAND_LOAD 			= LANGUAGE_ENGLISH.equals(language) ? "LOAD" : "CHARGER";
		COMMAND_EXIT 			= LANGUAGE_ENGLISH.equals(language) ? "EXIT" : "QUITTER";
		COMMAND_GRID			= LANGUAGE_ENGLISH.equals(language) ? "GRID" : "GRILLE";

		KEYWORD_BLACK			= LANGUAGE_ENGLISH.equals(language) ? "BLACK" : "NOIR";
		KEYWORD_RED				= LANGUAGE_ENGLISH.equals(language) ? "RED" : "ROUGE";
		KEYWORD_GREEN			= LANGUAGE_ENGLISH.equals(language) ? "GREEN" : "VERT";
		KEYWORD_YELLOW			= LANGUAGE_ENGLISH.equals(language) ? "YELLOW" : "JAUNE";
		KEYWORD_BLUE			= LANGUAGE_ENGLISH.equals(language) ? "BLUE" : "BLEU";
		KEYWORD_MAGENTA			= LANGUAGE_ENGLISH.equals(language) ? "MAGENTA" : "MAGENTA";
		KEYWORD_CYAN			= LANGUAGE_ENGLISH.equals(language) ? "CYAN" : "CYAN";
		KEYWORD_WHITE			= LANGUAGE_ENGLISH.equals(language) ? "WHITE" : "BLANC";
		KEYWORD_GRAY			= LANGUAGE_ENGLISH.equals(language) ? "GRAY" : "GRIS";
		KEYWORD_DARKGRAY		= LANGUAGE_ENGLISH.equals(language) ? "DARKGRAY" : "GRISFONCE";

		ITEM_FIRST				= LANGUAGE_ENGLISH.equals(language) ? "First of all..." : "Tout d'abord...";
		ITEM_INSTRUCTION		= LANGUAGE_ENGLISH.equals(language) ? "Instructions" : "Instructions";
		ITEM_COLOR				= LANGUAGE_ENGLISH.equals(language) ? "Colors" : "Couleurs";
		ITEM_EXPRESSION			= LANGUAGE_ENGLISH.equals(language) ? "Expressions" : "Expressions";
		ITEM_VARIABLE			= LANGUAGE_ENGLISH.equals(language) ? "Variables" : "Variables";
		ITEM_COMMAND			= LANGUAGE_ENGLISH.equals(language) ? "Commands" : "Commandes";
		ITEM_EXAMPLE			= LANGUAGE_ENGLISH.equals(language) ? "Examples" : "Exemples";

		UNIT_COLOR				= LANGUAGE_ENGLISH.equals(language) ? "color" : "couleur";
		UNIT_USAGE				= LANGUAGE_ENGLISH.equals(language) ? "Usage" : "Utilisation";
		UNIT_LANGUAGE_VERB		= LANGUAGE_ENGLISH.equals(language) ? "Verb-based language" : "Langage à base de verbes";
		UNIT_LANGUAGE_GEOMETRY	= LANGUAGE_ENGLISH.equals(language) ? "Geometric-based language" : "Langage à base de formes géométriques";

		MSG_HELLO				= LANGUAGE_ENGLISH.equals(language) ?
				"Hello. Are you ready to enter or load your program?" :
				"Bonjour. Es-tu prêt(e) à entrer ou charger ton programme ?";
		MSG_CONFIRM_EXIT		= LANGUAGE_ENGLISH.equals(language) ?
				"The program has been changed. Do you confirm your wish to leave JEMMA and therefore lose your modifications?" :
				"Le programme a été modifié. Confirmes-tu ton souhait de quitter JEMMA et donc de perdre tes modifications ?";
		MSG_CONFIRM_LOAD		= LANGUAGE_ENGLISH.equals(language) ?
				"The program has been changed. Do you confirm your wish to load another program and therefore lose your modifications?" :
				"Le programme a été modifié. Confirmes-tu ton souhait de charger un autre programme et donc de perdre tes modifications ?";
		MSG_CONFIRM_EMPTY		= LANGUAGE_ENGLISH.equals(language) ?
				"The program is empty. Do you confirm your wish to save it?" :
				"Le programme est vide. Confirmes-tu ton souhait de le sauvegarder ?";
		MSG_EXISTING_FILE		= LANGUAGE_ENGLISH.equals(language) ?
				"The file already exists. Do you want to replace it?" :
				"Le fichier existe déjà. Veux-tu le remplacer ?";
		MSG_CLEAN_DONE			= LANGUAGE_ENGLISH.equals(language) ? "Screen cleared." : "Ecran effacé.";
		MSG_LOAD_DONE			= LANGUAGE_ENGLISH.equals(language) ? "Program loaded." : "Programme chargé.";
		MSG_SAVE_DONE			= LANGUAGE_ENGLISH.equals(language) ? "Program saved." : "Programme enregistré.";
		MSG_START_CHECKING		= LANGUAGE_ENGLISH.equals(language) ? "Start checking program." : "Vérification de la syntaxe du programme.";
		MSG_PRG_CORRECT			= LANGUAGE_ENGLISH.equals(language) ? "Program syntactically correct." : "Programme syntaxiquement correct.";
		MSG_PRG_EMPTY			= LANGUAGE_ENGLISH.equals(language) ? "The program has no instructions." : "Le programme ne comporte aucune instruction.";
		MSG_LINE				= LANGUAGE_ENGLISH.equals(language) ? "Line: " : "Ligne : ";
		MSG_START_RUNNING		= LANGUAGE_ENGLISH.equals(language) ? "Start running program." : "Exécution du programme.";
		MSG_PRG_EXECUTED		= LANGUAGE_ENGLISH.equals(language) ? "Program syntactically executed." : "Programme exécuté.";
		MSG_PRG_ONGOING			= LANGUAGE_ENGLISH.equals(language) ? "Program execution on going (step by step)." : "Programme en cours d'exécution (pas à pas).";
		MSG_PRG_STOPDEBUG		= LANGUAGE_ENGLISH.equals(language) ? "Program debugging stopped." : "Arrêt du débogage du programme.";
		MSG_VERB_LANGUAGE		= LANGUAGE_ENGLISH.equals(language) ? "You selected the verb-based language." : "Tu as choisi le language à base de verbes.";
		MSG_GEOMETRY_LANGUAGE	= LANGUAGE_ENGLISH.equals(language) ? "You selected the geometric-based language." : "Tu as choisi le language à base de formes géométriques.";
		MSG_CONFIRM				= LANGUAGE_ENGLISH.equals(language) ?
				"You change programming language. The program currently displayed will be incorrect. Do you confirm this change?" :
				"Tu changes de language de programmation. Le programme actuellement affiché sera incorrect. Confirmes-tu ce changement ?";

		TITLE_LOAD				= LANGUAGE_ENGLISH.equals(language) ? "Program loading" : "Chargement d'un programme";
		TITLE_SAVE				= LANGUAGE_ENGLISH.equals(language) ? "Program saving" : "Enregistrement d'un programme";
		TITLE_CONFIRM			= LANGUAGE_ENGLISH.equals(language) ? "Confirm" : "Confirmation";

		INTERNALERROR			= LANGUAGE_ENGLISH.equals(language) ? "INTERNAL ERROR" : "ERREUR INTERNE";
		ERROR_READ_FILE			= LANGUAGE_ENGLISH.equals(language) ?
				"Application error while reading a file. Note the following information and send it to the JEMMA manager. Thanks in advance." :
				"Erreur de l'application pendant la lecture d'un fichier. Relève les informations qui suivent et transmet les au responsable de JEMMA. Merci par avance.";
		ERROR_WRITE_FILE		= LANGUAGE_ENGLISH.equals(language) ?
				"Application error while writing a file. Note the following information and send it to the JEMMA manager. Thanks in advance." :
				"Erreur de l'application pendant l'écriture d'un fichier. Relève les informations qui suivent et transmet les au responsable de JEMMA. Merci par avance.";
		ERROR_UNKNOWN_INST		= LANGUAGE_ENGLISH.equals(language) ? " - Unknown (part of) instruction." : " - Instruction (ou partie d') inconnue.";
		ERROR_END_OF_INST		= LANGUAGE_ENGLISH.equals(language) ? " - Incorrect end of instruction." : " - Fin d'nstruction incorrecte.";
		ERROR_EXPRESSION		= LANGUAGE_ENGLISH.equals(language) ? " - Incorrect expression." : " - Expression incorrecte.";
		ERROR_DIRECTION			= LANGUAGE_ENGLISH.equals(language) ? " - Incorrect direction." : " - Direction incorrecte.";
		ERROR_COLOR				= LANGUAGE_ENGLISH.equals(language) ? " - Incorrect color." : " - Couleur incorrecte.";
		ERROR_VARIABLE			= LANGUAGE_ENGLISH.equals(language) ? " - Incorrect variable name." : " - Nom de variable incorrect.";
		ERROR_DIVIDE			= LANGUAGE_ENGLISH.equals(language) ? " - Cannot divide by 0." : " - Division par 0.";
		ERROR_INCRCT_INST		= LANGUAGE_ENGLISH.equals(language) ? " - Incorrect instruction." : " - Instruction incorrecte.";
		ERROR_EXEC_STOP			= LANGUAGE_ENGLISH.equals(language) ?
				"Program execution stopped after the detection of first error." :
				"Execution du programme interrompue après détection de la première erreur.";
		ERROR_SOUND				= LANGUAGE_ENGLISH.equals(language) ? "Cannot manage the sound " : "Impossible de gérer le son ";
		ERROR_WALL				= LANGUAGE_ENGLISH.equals(language) ? "You made a mistake ... and you paid yourself a wall": "Tu as fais une erreur... et tu t'es payé(e) un mur !!!";
		ERROR_NESTED_LOOPS		= LANGUAGE_ENGLISH.equals(language) ?
				" - Misuse of the loop instructions." :
				" - Mauvaise utilisation de l'instruction de boucle.";
		ERROR_LOOP_NULL			=  LANGUAGE_ENGLISH.equals(language) ?
				" - Loop counter cannot be zero at the start of the loop." :
				" - Le compteur de boucle ne peut être à zéro au départ de la boucle.";
		WARNING_VAR_UNDEFINED	= LANGUAGE_ENGLISH.equals(language) ? "Variable not declared: " : "Variable non déclarée : ";

		USAGE_COMMAND				= "\n" + (LANGUAGE_ENGLISH.equals(language) ?
				"Commands are:\n\n" +
						"\t\uFFFA" + COMMAND_CHECK + "\uFFFB:\t\tTo check if your program is written correctly, that is to say that it is syntactically correct. That doesn't mean it will do what you want.\n" +
						"\t\uFFFA" + COMMAND_RUN  + "\uFFFB:\t\t\tTo run your program. The result will therefore be displayed in the right window.\n" +
						"\t\uFFFA" + COMMAND_DEBUG + "\uFFFB:\t\tTo run your program, but instruction by instruction. This will allow you to see where the error is, for example where you enter the wall... Click on the small buttons below to either execute the following instruction or to stop execution.\n" +
						"\t\uFFFA" + COMMAND_CLEAR + "\uFFFB:\t\tTo erase the last drawing, ie the traces of the last execution.\n" +
						"\t\uFFFA" + COMMAND_GRID + "\uFFFB:\t\t\tTo display a tracing aid grid. A square is the equivalent of 20 steps.\n" +
						"\t\uFFFA" + COMMAND_SAVE + "\uFFFB:\t\t\tTo save your program to a file on disk. You can then reuse it.\n" +
						"\t\uFFFA" + COMMAND_SAVEAS + "\uFFFB:\tItou than the previous command but you will have to explicitly name the result file.\n" +
						"\t\uFFFA" + COMMAND_LOAD  + "\uFFFB:\t\tTo load (use) an existing program that you may have saved to disk.\n" +
						"\t\uFFFA" + COMMAND_EXIT + "\uFFFB:\t\t\tTo leave JEMMA....\n" :
				"Les commandes sont :\n\n" +
						"\t\uFFFA" + COMMAND_CHECK + "\uFFFB :\t\t\t\t\t\tPour vérifier si ton programme est écrit correctement, c'est à dire qu'il est syntaxiquement correct. Cela ne veut pas dire qu'il fera ce que tu voudrais qu'il fasse.\n" +
						"\t\uFFFA" + COMMAND_RUN + "\uFFFB :\t\t\t\t\tPour exécuter ton programme. Le résultat sera donc affiché dans la fenêtre de droite.\n" +
						"\t\uFFFA" + COMMAND_DEBUG + "\uFFFB :\t\t\t\tPour exécuter ton programme, mais instruction par instruction. Cela permettra de voir où est l'erreur, par exemple là où tu entres dans le mur... Cliquer sur les petits boutons en-dessous pour soit exécuter l'instruction suivante soit pour arrêter l'exécution.\n" +
						"\t\uFFFA" + COMMAND_CLEAR + "\uFFFB :\t\t\t\t\t\tPour effacer le dernier dessin, c'est à dire les traces de la dernière exécution.\n" +
						"\t\uFFFA" + COMMAND_GRID + "\uFFFB :\t\t\t\t\t\t\tPour afficher une grille d'aide au traçage. Un carré est l'équivalent de 20 pas.\n" +
						"\t\uFFFA" + COMMAND_SAVE + "\uFFFB :\t\t\t\tPour enregistrer ton programme dans un fichier sur le disque. Tu pourras ainsi le réutiliser.\n" +
						"\t\uFFFA" + COMMAND_SAVEAS + "\uFFFB :\tItou que la commande précédente mais tu devras explicitement tnommer le fichier résultat.\n" +
						"\t\uFFFA" + COMMAND_LOAD  + "\uFFFB :\t\t\t\t\t\tPour charger (utiliser) un programme existant que tu as peut-être enregistré sur le disque.\n" +
						"\t\uFFFA" + COMMAND_EXIT + "\uFFFB :\t\t\t\t\t\t\tPour quitter JEMMA....\n");
		USAGE_EXPRESSION			= "\n" + (LANGUAGE_ENGLISH.equals(language) ?
				"An expression is a combination of following operations, may be between parenthesis. An operand is a variable or a number.\n\n" +
						"\t\uFFFAAddition\uFFFB:\t\t\t\toperand + operand\n" +
						"\t\uFFFASubstraction\uFFFB:\t\toperand - operand\n" +
						"\t\uFFFAMultiplication\uFFFB:\t\toperand * operand\n" +
						"\t\uFFFADivision\uFFFB:\t\t\t\toperand / operand\n\n" +
						"Examples: N + 3 or (N + 3) * 2" :
				"Une expression est une combinaison des opérations suivantes, peut-être entre parenthèses. Un opérande est une variable ou un nombre.\n\n" +
						"\t\uFFFAAddition\uFFFB:\t\t\t\topérande + opérande\n" +
						"\t\uFFFASoustraction\uFFFB:\t\topérande - opérande\n" +
						"\t\uFFFAMultiplication\uFFFB:\t\topérande * opérande\n" +
						"\t\uFFFADivision\uFFFB:\t\t\t\topérande / opérande\n\n" +
						"Exemples : N + 3 ou (N + 3) * 2");
		USAGE_VARIABLE			= "\n" + (LANGUAGE_ENGLISH.equals(language) ?
				"\uFFFAVariables\uFFFB are symbols that associate a name (identifier) with a value. The name is unique but the value may change over time. For the name of a variable:\n\n" +
						"\t- there can only be lowercase, uppercase and numbers;\n" +
						"\t- the name of the variable must start with a letter;\n" +
						"\t- spaces are prohibited. Instead, you can use the underscore character _;\n" +
						"\t- you are not allowed to use accents.\n" :
				"Les \uFFFAvariables\uFFFB sont des symboles qui associent un nom (l'identifiant) à une valeur. Le nom est unique mais la valeur peut changer au cours du temps. Pour le nom d'une variable :\n\n" +
						"\t- il ne peut y avoir que des minuscules, majuscules et des chiffres;\n" +
						"\t- le nom de la variable doit commencer par une lettre;\n" +
						"\t- les espaces sont interdits. À la place, on peut utiliser le caractère « underscore » _;\n" +
						"\t- tu n'as pas le droit d'utiliser des accents.\n");
		USAGE_EXAMPLE				= "\n" + (LANGUAGE_ENGLISH.equals(language) ?
				"\uFFFASample programs\uFFFB to load are provided in the following directory:\n\n" :
				"Des \uFFFAprogrammes d'exemples\uFFFB à charger sont fournis dans le répertoire suivant :\n\n") +
				"\t\t";

		colorsTab = new String[]{
				KEYWORD_BLACK,
				KEYWORD_BLUE,
				KEYWORD_CYAN,
				KEYWORD_GRAY,
				KEYWORD_DARKGRAY,
				KEYWORD_GREEN,
				KEYWORD_MAGENTA,
				KEYWORD_RED,
				KEYWORD_WHITE,
				KEYWORD_YELLOW
		};
	}
}
