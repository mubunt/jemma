//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import java.io.IOException;

public class jemmaSounds {
	private static final String unixMusicPlayer	= "mpg123";
	private static final String windowsMusicPlayer = "cmdmp3";

	static void playSound(String fsound) {
		String soundCommand = ((System.getProperty("os.name").toLowerCase().contains("win")) ?
				jemma.installRootDir + windowsMusicPlayer : unixMusicPlayer) +
				" " + jemma.installRootDir + fsound;
		try {
			Runtime.getRuntime().exec(soundCommand);
		} catch (IOException e) {
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.ERROR_SOUND, jemmaConsole.msgType.ERROR);
		}
	}
}
//------------------------------------------------------------------------------
