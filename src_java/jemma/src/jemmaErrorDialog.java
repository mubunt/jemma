//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.widgets.Display;

import java.util.ArrayList;
import java.util.List;

public class jemmaErrorDialog {
	static MultiStatus createMultiStatus(Throwable t) {
		List<Status> childStatuses = new ArrayList<>();
		StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();
		for (StackTraceElement stackTrace: stackTraces) {
			Status status = new Status(IStatus.ERROR, "jemma.jar", stackTrace.toString());
			childStatuses.add(status);
		}
		return new MultiStatus("jemma.jar",
				IStatus.ERROR, childStatuses.toArray(new Status[] {}),
				t.toString(), t);
	}
	static void error(String title, String message, MultiStatus status ) {
		ErrorDialog.openError(Display.getCurrent().getActiveShell(), title, message, status);
	}
}
