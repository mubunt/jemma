//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

public class jemmaConsole {
	enum msgType { INFORMATION, WARNING, ERROR }

	private static final Color red = Display.getDefault().getSystemColor(SWT.COLOR_RED);
	private static final Color yellow = Display.getDefault().getSystemColor(SWT.COLOR_YELLOW);
	private static final Color myDarkGreen = new Color(Display.getDefault(), 0, 41, 0);
	private static final Color myLightGreen = new Color(Display.getDefault(), 0, 230, 0);

	private static final Color consoleBackground = myDarkGreen;
	private static final Color consoleForeground = myLightGreen;
	private static final Font consoleFont = new Font(Display.getCurrent(), "Arial", 9, SWT.NORMAL);
	private static StyledText console;

	static StyledText getConsole( Composite parent ) {
		console = new StyledText(parent, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL | SWT.WRAP);
		console.setBackground(consoleBackground);
		console.setForeground(consoleForeground);
		console.setFont(consoleFont);
		return console;
	}
	//--------------------------------------------------------------------------
	static void writeConsole( String prompt, String msg, msgType m ) {
		if (console == null) return;
		int start = console.getCharCount();
		console.append(prompt + msg + jemmaUI.EOL);
		StyleRange styleRange = new StyleRange();
		styleRange.start = start;
		styleRange.length = msg.length() + prompt.length() + jemmaUI.EOL.length();
		switch (m) {
			case WARNING:
				styleRange.fontStyle = SWT.NORMAL;
				styleRange.foreground = yellow;
				break;
			case ERROR:
				styleRange.fontStyle = SWT.BOLD;
				styleRange.foreground = red;
				break;
			case INFORMATION:
				styleRange.fontStyle = SWT.NORMAL;
				styleRange.foreground = consoleForeground;
			default:
				break;
		}
		console.setStyleRange(styleRange);
		console.setSelection(console.getText().length());
	}
	//--------------------------------------------------------------------------
	static void exit() {
		consoleFont.dispose();
		myDarkGreen.dispose();
		myLightGreen.dispose();
	}
	//--------------------------------------------------------------------------
}
