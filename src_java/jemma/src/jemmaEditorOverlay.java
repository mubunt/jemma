//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// A customizable overlay over a control.
//
// Adapted (very few modifications) from a code written by Loris Securo and found at the following address:
// 		https://stackoverflow.com/questions/42087977/place-transparent-composite-over-other-composite-in-swt/42286915
// Thanks to the author
//
//  @author Loris Securo
//------------------------------------------------------------------------------

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class jemmaEditorOverlay {
	private final List<Composite> parents;
	private final Control objectToOverlay;
	private final int widthOverlay;
	private final int heightOverlay;
	private final Shell overlay;
	private final Label label;
	private final ControlListener controlListener;
	private final DisposeListener disposeListener;
	private final PaintListener paintListener;
	private boolean showing;
	private final boolean hasClientArea;
	private final Scrollable scrollableToOverlay;

	public jemmaEditorOverlay(Control objectToOverlay, int width, int height) {
		Objects.requireNonNull(objectToOverlay);
		this.objectToOverlay = objectToOverlay;
		this.widthOverlay = width;
		this.heightOverlay = height;
		// if the object to overlay is an instance of Scrollable (e.g. Shell) then it has
		// the getClientArea method, which is preferable over Control.getSize
		if (objectToOverlay instanceof Scrollable) {
			hasClientArea = true;
			scrollableToOverlay = (Scrollable) objectToOverlay;
		} else {
			hasClientArea = false;
			scrollableToOverlay = null;
		}
		// save the parents of the object, so we can add/remove listeners to them
		parents = new ArrayList<>();
		Composite parent = objectToOverlay.getParent();
		while (parent != null) {
			parents.add(parent);
			parent = parent.getParent();
		}
		// listener to track position and size changes in order to modify the overlay bounds as well
		controlListener = new ControlListener() {
			@Override
			public void controlMoved(ControlEvent e) {
				reposition();
			}
			@Override
			public void controlResized(ControlEvent e) {
				reposition();
			}
		};
		// listener to track paint changes, like when the object or its parents become not visible (for example changing tab in a TabFolder)
		paintListener = arg0 -> reposition();
		// listener to remove the overlay if the object to overlay is disposed
		disposeListener = e -> remove();
		// create the overlay shell
		overlay = new Shell(objectToOverlay.getShell(), SWT.NO_TRIM);
		// default values of the overlay
		overlay.setBackground(objectToOverlay.getDisplay().getSystemColor(SWT.COLOR_GRAY));
		overlay.setForeground(objectToOverlay.getDisplay().getSystemColor(SWT.COLOR_RED));
		overlay.setAlpha(250);
		// so the label can inherit the background of the overlay
		overlay.setBackgroundMode(SWT.INHERIT_DEFAULT);
		// label to display a text
		// style WRAP so if it is too long the text get wrapped
		label = new Label(overlay, SWT.WRAP);
		// to center the label
		overlay.setLayout(new GridLayout());
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		showing = false;
		overlay.open();
		overlay.setVisible(showing);
	}
	public void show() {
		// if it's already visible we just exit
		if (showing) return;
		// set the overlay position over the object
		reposition();
		// show the overlay
		overlay.setVisible(true);
		// add listeners to the object to overlay
		objectToOverlay.addControlListener(controlListener);
		objectToOverlay.addDisposeListener(disposeListener);
		objectToOverlay.addPaintListener(paintListener);
		// add listeners also to the parents because if they change then also the visibility of our object could change
		for (Composite parent : parents) {
			parent.addControlListener(controlListener);
			parent.addPaintListener(paintListener);
		}
		showing = true;
	}
	public void remove() {
		// if it's already not visible we just exit
		if ( ! showing) return;
		// remove the listeners
		if (!objectToOverlay.isDisposed()) {
			objectToOverlay.removeControlListener(controlListener);
			objectToOverlay.removeDisposeListener(disposeListener);
			objectToOverlay.removePaintListener(paintListener);
		}
		// remove the parents listeners
		for (Composite parent : parents) {
			if (!parent.isDisposed()) {
				parent.removeControlListener(controlListener);
				parent.removePaintListener(paintListener);
			}
		}
		// remove the overlay shell
		if (!overlay.isDisposed()) overlay.setVisible(false);
		showing = false;
	}
//	public void setBackground(Color background) {
//		overlay.setBackground(background);
//	}
//	public Color getBackground() {
//		return overlay.getBackground();
//	}
//	public void setAlpha(int alpha) {
//		overlay.setAlpha(alpha);
//	}
//	public int getAlpha() {
//		return overlay.getAlpha();
//	}
	public boolean isShowing() {
		return showing;
	}
	public void setText(String text) {
		label.setText(text);
		// to adjust the label size accordingly
		overlay.layout();
	}
//	public String getText() {
//		return label.getText();
//	}
	private void reposition() {
		// if the object is not visible, we hide the overlay and exit
		if ( ! objectToOverlay.isVisible()) {
			overlay.setBounds(new Rectangle(0, 0, 0, 0));
			return;
		}
		// if the object is visible we need to find the visible region in order to correctly place the overlay
		// get the display bounds of the object to overlay
		Point objectToOverlayDisplayLocation = objectToOverlay.toDisplay(0, 0);
		Point objectToOverlaySize;
		// if it has a client area, we prefer that instead of the size
		if (hasClientArea) {
			Rectangle clientArea = scrollableToOverlay.getClientArea();
			objectToOverlaySize = new Point(clientArea.width, clientArea.height);
		}
		else {
			objectToOverlaySize = objectToOverlay.getSize();
		}
		int objectDisplaytMarge = 10;
		Rectangle intersection = new Rectangle(
				objectToOverlayDisplayLocation.x + objectToOverlaySize.x - widthOverlay - objectDisplaytMarge,
				objectToOverlayDisplayLocation.y + objectDisplaytMarge,
				widthOverlay, heightOverlay);
		// intersect the bounds of the object with its parents bounds so we get only the visible bounds
		for (Composite parent : parents) {
			Rectangle parentClientArea = parent.getClientArea();
			Point parentLocation = parent.toDisplay(parentClientArea.x, parentClientArea.y);
			Rectangle parentBounds = new Rectangle(parentLocation.x, parentLocation.y, parentClientArea.width, parentClientArea.height);
			intersection = intersection.intersection(parentBounds);
			// if intersection has no size then it would be a waste of time to continue
			if (intersection.width == 0 || intersection.height == 0) break;
		}
		overlay.setBounds(intersection);
/*
		//define a region that looks like a rounded rectangle
		Region region = createRoundedRectangle(intersection.x, intersection.y, intersection.width, intersection.height, 5);
		//define the shape of the shell using setRegion
		overlay.setRegion(region);
		region.dispose();
*/
	}
/*
	//--------------------------------------------------------------------------
	// Code from https://stackoverflow.com/questions/22431269/swt-shell-with-round-corners
	private static Region createRoundedRectangle(int x, int y, int W, int H, int r) {
		Region region = new Region();
		int d = (2 * r); // diameter
		region.add(circle(r, (x + r), (y + r)));
		region.add(circle(r, (x + W - r), (y + r)));
		region.add(circle(r, (x + W - r), (y + H - r)));
		region.add(circle(r, (x + r), (y + H - r)));
		region.add((x + r), y, (W - d), H);
		region.add(x, (y + r), W, (H - d));
		return region;
	}
	private static int[] circle(int r, int offsetX, int offsetY) {
		int[] polygon = new int[8 * r + 4];
		// x^2 + y^2 = r^2
		for (int i = 0; i < 2 * r + 1; i++) {
			int x = i - r;
			int y = (int) Math.sqrt(r * r - x * x);
			polygon[2 * i] = offsetX + x;
			polygon[2 * i + 1] = offsetY + y;
			polygon[8 * r - 2 * i - 2] = offsetX + x;
			polygon[8 * r - 2 * i - 1] = offsetY - y;
		}
		return polygon;
	}
*/
	//--------------------------------------------------------------------------
}