//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;

import java.io.*;

public class jemmaCommand {
	static int previous_executed_line;
	static void load_program( File file ) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		String st;
		jemmaUI.editor.setText("");
		while ((st = br.readLine()) != null)
			jemmaUI.editor.append(st + jemmaUI.EOL);
		br.close();
	}
	static void save_program( File file, String program ) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		bw.write(program);
		bw.close();
	}
}

class command_check implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_START_CHECKING, jemmaConsole.msgType.INFORMATION);
		if (jemmaUI.editor.getLineCount() == 0) {
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_PRG_EMPTY, jemmaConsole.msgType.ERROR);
			return;
		}
		if (jemmaExecution.analyzer(false))
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_PRG_CORRECT, jemmaConsole.msgType.INFORMATION);
		jemmaUI.possibleCommandAfter(jemmaUI.context.CHECKED);
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_run implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_START_RUNNING, jemmaConsole.msgType.INFORMATION);
		jemmaExecution.clearDrawing();
		if (jemmaUI.editor.getLineCount() == 0) {
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_PRG_EMPTY, jemmaConsole.msgType.ERROR);
			return;
		}
		if (jemmaExecution.analyzer(true))
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_PRG_EXECUTED, jemmaConsole.msgType.INFORMATION);
		jemmaUI.possibleCommandAfter(jemmaUI.context.EXECUTED);
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_debug implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		if (jemmaUI.editor.getLineCount() == 0) {
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_PRG_EMPTY, jemmaConsole.msgType.ERROR);
			return;
		}
		jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_PRG_ONGOING, jemmaConsole.msgType.INFORMATION);
		jemmaExecution.clearDrawing();
		jemmaCommand.previous_executed_line = -1;
		jemmaExecution.stepByStepExecutionStart();
		jemmaUI.possibleCommandAfter(jemmaUI.context.DEBUGGED);
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_debugNext implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		if  (jemmaExecution.lineno < (jemmaUI.editor.getLineCount() - 1)) {
			jemmaConsole.writeConsole(jemma.Prompt, "Instruction " + (jemmaExecution.lineno + 1), jemmaConsole.msgType.INFORMATION);
			if (jemmaCommand.previous_executed_line != -1)
				jemmaUI.unhighLightLine(jemmaCommand.previous_executed_line);
			jemmaCommand.previous_executed_line = jemmaExecution.lineno;
			jemmaUI.highLightLine(jemmaExecution.lineno);
			if (! jemmaExecution.stepByStepExecutionNext()) {
				jemmaExecution.displayError(-1, jemmaLanguage.ERROR_EXEC_STOP);
				jemmaUI.possibleCommandAfter(jemmaUI.context.STOPDEBUGGED);
			}
			jemmaUI.possibleCommandAfter(jemmaUI.context.NEXTDEBUGGED);
		} else {
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_PRG_STOPDEBUG, jemmaConsole.msgType.INFORMATION);
			jemmaUI.possibleCommandAfter(jemmaUI.context.STOPDEBUGGED);
		}
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_debugStop implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		if (jemmaCommand.previous_executed_line != -1)
			jemmaUI.unhighLightLine(jemmaCommand.previous_executed_line);
		jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_PRG_STOPDEBUG, jemmaConsole.msgType.INFORMATION);
		jemmaUI.possibleCommandAfter(jemmaUI.context.STOPDEBUGGED);
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_clear implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		jemmaExecution.clearDrawing();
		jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_CLEAN_DONE, jemmaConsole.msgType.INFORMATION);
		jemmaUI.possibleCommandAfter(jemmaUI.context.CLEANED);
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_save implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		String program = jemmaUI.editor.getText();
		if (program.isEmpty()) {
			boolean b = MessageDialog.openConfirm(
					Display.getCurrent().getActiveShell(),
					jemmaLanguage.COMMAND_SAVE,
					jemmaLanguage.MSG_CONFIRM_EMPTY);
			if (! b) return;
		}
		File file;
		if (jemma.programDirectory.length() != 0 && jemma.programName.length() != 0) {
			file = new File(jemma.programDirectory + "/" + jemma.programName);
		} else {
			FileDialog fd = new FileDialog(Display.getCurrent().getActiveShell(), SWT.SAVE);
			fd.setText(jemmaLanguage.TITLE_SAVE);
			fd.setFilterPath(jemma.programDirectory);
			fd.setFileName(jemma.programName);
			String[] filterExt = {"*." + jemma.suffixLanguage};
			fd.setFilterExtensions(filterExt);
			String selected = fd.open();
			if (selected == null || selected.isEmpty()) return;
			file = new File(selected);
			if (file.exists()) {
				boolean b = MessageDialog.openConfirm(
						Display.getCurrent().getActiveShell(),
						jemmaLanguage.COMMAND_SAVE,
						jemmaLanguage.MSG_EXISTING_FILE);
				if (!b) return;
			}
			jemma.programDirectory = file.getParent();
			jemma.programName = file.getName();
		}
		try {
			jemmaCommand.save_program(file, program);
		} catch (IOException e) {
			MultiStatus status = jemmaErrorDialog.createMultiStatus(e);
			jemmaErrorDialog.error(
					jemmaLanguage.INTERNALERROR,
					jemmaLanguage.ERROR_WRITE_FILE,
					status);
		}
		jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_SAVE_DONE + "(" + jemma.programName + ")",
				jemmaConsole.msgType.INFORMATION);
		jemmaUI.possibleCommandAfter(jemmaUI.context.SAVED);
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_saveas implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		String program = jemmaUI.editor.getText();
		if (program.isEmpty()) {
			boolean b = MessageDialog.openConfirm(
					Display.getCurrent().getActiveShell(),
					jemmaLanguage.COMMAND_SAVEAS,
					jemmaLanguage.MSG_CONFIRM_EMPTY);
			if (! b) return;
		}
		FileDialog fd = new FileDialog(Display.getCurrent().getActiveShell(), SWT.SAVE);
		fd.setText(jemmaLanguage.TITLE_SAVE);
		fd.setFilterPath(jemma.programDirectory);
		fd.setFileName(jemma.programName);
		String[] filterExt = {"*." + jemma.suffixLanguage};
		fd.setFilterExtensions(filterExt);
		String selected = fd.open();
		if (selected == null || selected.isEmpty()) return;
		File file = new File(selected);
		if (file.exists()) {
			boolean b = MessageDialog.openConfirm(
					Display.getCurrent().getActiveShell(),
					jemmaLanguage.COMMAND_SAVE,
					jemmaLanguage.MSG_EXISTING_FILE);
			if (!b) return;
		}
		jemma.programDirectory = file.getParent();
		jemma.programName = file.getName();
		try {
			jemmaCommand.save_program(file, program);
		} catch (IOException e) {
			MultiStatus status = jemmaErrorDialog.createMultiStatus( e);
			jemmaErrorDialog.error(
					jemmaLanguage.INTERNALERROR,
					jemmaLanguage.ERROR_WRITE_FILE,
					status);
		}
		jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_SAVE_DONE + "(" + jemma.programName + ")",
				jemmaConsole.msgType.INFORMATION);
		jemmaUI.possibleCommandAfter(jemmaUI.context.SAVED);
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_load implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		if (jemmaUI.isProgramModified()) {
			boolean b = MessageDialog.openConfirm(
					Display.getCurrent().getActiveShell(),
					jemmaLanguage.COMMAND_LOAD,
					jemmaLanguage.MSG_CONFIRM_LOAD);
			if (! b) return;
		}
		FileDialog fd = new FileDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);
		fd.setText(jemmaLanguage.TITLE_LOAD);
		fd.setFilterPath(jemma.programDirectory);
		String[] filterExt = {"*." + jemma.suffixLanguage};
		fd.setFilterExtensions(filterExt);
		String selected = fd.open();
		if (selected == null || selected.isEmpty()) return;
		File file = new File(selected);
		jemma.programDirectory = file.getParent();
		jemma.programName = file.getName();
		try {
			jemmaCommand.load_program(file);
		} catch (IOException e) {
			MultiStatus status = jemmaErrorDialog.createMultiStatus(e);
			jemmaErrorDialog.error(
					jemmaLanguage.INTERNALERROR,
					jemmaLanguage.ERROR_READ_FILE,
					status);
		}
		jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.MSG_LOAD_DONE + "(" + jemma.programName + ")",
				jemmaConsole.msgType.INFORMATION);
		jemmaUI.possibleCommandAfter(jemmaUI.context.LOADED);
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_exit implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		jemma.Exit(0);
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}

class command_grid implements SelectionListener {
	public void widgetSelected(SelectionEvent event) {
		Listener listener1 = event1 -> {
			GC gc = event1.gc;
			gc.setAlpha(70);
			gc.setLineWidth(1);
			gc.setForeground(jemmaUI.black);
			gc.setBackground(jemmaUI.executionarea_background);
			gc.setFont(jemmaUI.fontGrid);
			gc.setXORMode(false);
			final int interval = 20;
			int n = jemmaUI.ExecutionArea.getBounds().height / interval;
			int y = 0;
			for (int i = 0; i < n; i++) {
				y = y + interval;
				gc.drawLine(0, y, jemmaUI.ExecutionArea.getBounds().width, y);
				gc.drawText("" + y, 2, y + 2);
			}
			n = jemmaUI.ExecutionArea.getBounds().width / interval;
			int x = 0;
			for (int i = 0; i < n; i++) {
				x = x + interval;
				gc.drawLine(x, 0, x, jemmaUI.ExecutionArea.getBounds().height);
				gc.drawText("" + x, x + 2, 2);
			}
			gc.setAlpha(255);
		};
		Listener listener2 = event1 -> {
			GC gc = event1.gc;
			gc.setAlpha(0);
			gc.setLineWidth(1);
			final int interval = 20;
			int n = jemmaUI.ExecutionArea.getBounds().height / interval;
			int y = 0;
			for (int i = 0; i < n; i++) {
				y = y + interval;
				gc.drawLine(0, y, jemmaUI.ExecutionArea.getBounds().width, y);
			}
			n = jemmaUI.ExecutionArea.getBounds().width / interval;
			int x = 0;
			for (int i = 0; i < n; i++) {
				x = x + interval;
				gc.drawLine(x, 0, x, jemmaUI.ExecutionArea.getBounds().height);
			}
			gc.setAlpha(255);
		};
		if (jemma.gridDisplayed) {
			jemmaUI.ExecutionArea.removeListener(SWT.Paint, listener1);
			jemmaUI.ExecutionArea.addListener(SWT.Paint, listener2);
		} else {
			jemmaUI.ExecutionArea.removeListener(SWT.Paint, listener2);
			jemmaUI.ExecutionArea.addListener(SWT.Paint, listener1);
		}
		jemmaUI.ExecutionArea.redraw();
		jemma.gridDisplayed = ! jemma.gridDisplayed;
	}
	public void widgetDefaultSelected(SelectionEvent event)  { }
}
//------------------------------------------------------------------------------
