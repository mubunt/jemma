//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.nebula.widgets.pshelf.PShelf;
import org.eclipse.nebula.widgets.pshelf.PShelfItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class jemmaDocumentation {
	private static final Color info_background = jemmaUI.mySlightlyLighterDarkGray;
	private static final Color info_selectionforeground = jemmaUI.white;
	private static final Color infoitem_background = jemmaUI.myFloralWhite;
	private static final Color infoitem_foreground = jemmaUI.black;
	private static final Color shellInformation_background = jemmaUI.gray;

	static void displayInformation( ) {
		final Shell shell = new Shell(jemmaUI.display);
		shell.setLayout(new FillLayout());
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setText(jemma.jemmaName + " - " + jemmaLanguage.UNIT_USAGE);
		shell.setBackground(shellInformation_background);

		PShelf shelf = new PShelf(shell, SWT.SIMPLE);
		shelf.setFont(jemmaUI.fontTitleInfo);
		shelf.setBackground(info_background);
		shelf.setForeground(info_selectionforeground);
		initShelf(shelf, jemmaLanguage.ITEM_FIRST, jemmaLanguage.USAGE_FIRST);
		initShelf(shelf, jemmaLanguage.ITEM_INSTRUCTION, jemmaLanguage.USAGE_INSTRUCTION);
		initShelf(shelf, jemmaLanguage.ITEM_EXPRESSION, jemmaLanguage.USAGE_EXPRESSION);
		initShelfColor(shelf, jemmaLanguage.ITEM_COLOR);
		initShelf(shelf, jemmaLanguage.ITEM_VARIABLE, jemmaLanguage.USAGE_VARIABLE);
		if (jemma.ProgrammingLanguage == jemmaLanguage.eLanguage.VERB)
			initShelf(shelf, jemmaLanguage_Verb.ITEM_DIRECTION, jemmaLanguage_Verb.USAGE_DIRECTION);
		initShelf(shelf, jemmaLanguage.ITEM_COMMAND, jemmaLanguage.USAGE_COMMAND);
		String s =  jemmaLanguage.USAGE_EXAMPLE + jemma.installRootDir + jemma.ExampleDir;
		initShelf(shelf, jemmaLanguage.ITEM_EXAMPLE, s);

		shell.setSize(700, 500);
		shell.open();
		while (!shell.isDisposed()) {
			if (!jemmaUI.display.readAndDispatch())
				jemmaUI.display.sleep();
		}
	}
	//--------------------------------------------------------------------------
	private static void initShelf(PShelf folder, String itemName, String textContent) {
		PShelfItem item = new PShelfItem(folder, SWT.NONE);
		item.setText(itemName);
		item.getBody().setLayout(new FillLayout());

		StyledText text = new StyledText(item.getBody(),SWT.READ_ONLY | SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		text.setFont(jemmaUI.fontInfo);
		text.setBackground(infoitem_background);
		text.setForeground(infoitem_foreground);
		text.setText(textContent);
		boldText(text);
	}
	static void boldText( StyledText text ) {
		int lastOffset = 0;
		int offset, offset2;
		do {
			String s = text.getText();
			offset = s.indexOf(jemma.startBeautifying, lastOffset);
			offset2 = s.indexOf(jemma.endBeautifying, lastOffset);
			if (offset != -1 && offset2 != -1) {
				text.replaceTextRange(offset2, 1, "");
				text.replaceTextRange(offset, 1, "");
				StyleRange style = new StyleRange();
				style.start = offset;
				style.length = offset2 - offset - 1;
				style.fontStyle = SWT.BOLD;
				style.foreground = jemmaUI.black;
				text.setStyleRange(style);
				lastOffset = offset2;
			}
		} while (offset != -1 || offset2 != -1);
	}
	//--------------------------------------------------------------------------
	private static void initShelfColor(PShelf folder, String itemName) {
		PShelfItem item = new PShelfItem(folder, SWT.NONE);
		item.setText(itemName);
		item.getBody().setLayout(new FillLayout());

		Table table = new Table(item.getBody(), SWT.BORDER);
		table.setBackground(infoitem_background);
		table.setForeground(infoitem_foreground);
		TableItem titem;
		TableColumn column1 = new TableColumn(table, SWT.NONE);
		TableColumn column2 = new TableColumn(table, SWT.NONE);
		for (int i = 0; i < jemmaLanguage.colorsTab.length; i++) {
			titem = new TableItem(table, SWT.NONE);
			titem.setText(new String[] { jemmaLanguage.colorsTab[i], " "});
			titem.setBackground(1, jemmaExecution.systemColors[i]);
		}
		column1.pack();
		column2.pack();
	}
}
//------------------------------------------------------------------------------
