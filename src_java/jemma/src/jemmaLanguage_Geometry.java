//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
public class jemmaLanguage_Geometry {
	static String KEYWORD_DIMENSION;
	static String KEYWORD_ANGLES;
	static String KEYWORD_TIMES;
	static String KEYWORD_TO;
	static String KEYWORD_TO2;
	static String KEYWORD_AND;
	static String KEYWORD_SOLID;
	static String KEYWORD_DOT;
	static String KEYWORD_DASH;
	static String UNIT_NO;
	static String UNIT_YES;

	static void init_language( String language ) {
		jemma.suffixLanguage 				= "jemmaG";

		String KEYWORD_ARC					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "ARC" : "ARC";
		String KEYWORD_ASSIGN				= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "ASSIGN" : "METTRE";
		String KEYWORD_DO					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "DO" : "EXECUTER";
		String KEYWORD_ENDDO				= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "END" : "FIN";
		String KEYWORD_FILL					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "FILL" : "REMPLIR";
		String KEYWORD_LINE					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "LINE" : "LIGNE";
		String KEYWORD_OVAL					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "OVAL" : "OVAL";
		String KEYWORD_RECTANGLE 			= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "RECTANGLE" : "RECTANGLE";
		String KEYWORD_STYLE				= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "STYLE" : "STYLE";
		String KEYWORD_TEXT					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "TEXT" : "TEXTE";
		String KEYWORD_THICKNESS			= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "THICKNESS" : "EPAISSEUR";
		String KEYWORD_XOR					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "XOR" : "XOR";
		String KEYWORD_RRECTANGLE			= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "ROUNDEDRECTANGLE" : "ZRECTANGLE";
		String KEYWORD_TRIANGLE				= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "TRIANGLE" : "TRIANGLE";
		KEYWORD_DIMENSION					= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "DIM" : "DIM";
		KEYWORD_ANGLES						= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "ANGLES" : "ANGLES";
		KEYWORD_TIMES 						= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "TIMES" : "FOIS";
		KEYWORD_TO							= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "TO" : "DANS";
		KEYWORD_TO2							= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "TO" : "JUSQU'A";
		KEYWORD_AND							= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "AND" : "ET";
		KEYWORD_SOLID						= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "SOLID" : "PLEIN";
		KEYWORD_DOT 						= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "DOT" : "POINT";
		KEYWORD_DASH 						= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "DASH" : "TIRET";
		UNIT_NO 							= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "NO" : "NON";
		UNIT_YES 							= jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "YES" : "OUI";

		String UNIT_TEXT					= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "\"text\"" : "\"texte\"") + "]";
		String UNIT_FILLING					= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "no|color" : "non|couleur") + "]";
		String UNIT_STYLE					= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "style" : "style") + "]";
		String UNIT_ANGLES					= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "startAngle,endAngle" : "angleDépart,angleArrivée") + "]";
		String UNIT_XOR						= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "mode" : "mode") + "]";
		String UNIT_COORDINATES				= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "x,y" : "x,y") + "]";
		String UNIT_DIMENSIONS				= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "width,height" : "largeur,hauteur") + "]";
		String UNIT_EXPRESSION				= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "expression" : "expression") + "]";
		String UNIT_VARIABLE				= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "variable" : "variable") + "]";
		String UNIT_COLOR					= "[" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ? "color" : "couleur") + "]";

		String SUITE_INSTRUCTION_ARC		= UNIT_COORDINATES + " " + KEYWORD_DIMENSION + " " + UNIT_DIMENSIONS + " " + KEYWORD_ANGLES + " " + UNIT_ANGLES;
		String SUITE_INSTRUCTION_ASSIGN		= UNIT_EXPRESSION + " " + KEYWORD_TO + " " + UNIT_VARIABLE;
		String SUITE_INSTRUCTION_DO			= UNIT_EXPRESSION + " " + KEYWORD_TIMES;
		String SUITE_INSTRUCTION_ENDDO		= "";
		String SUITE_INSTRUCTION_FILL		= UNIT_FILLING;
		String SUITE_INSTRUCTION_LINE		= UNIT_COORDINATES + " " + KEYWORD_TO2 + " " + UNIT_COORDINATES;
		String SUITE_INSTRUCTION_OVAL		= UNIT_COORDINATES + " " + KEYWORD_DIMENSION + " " + UNIT_DIMENSIONS;
		String SUITE_INSTRUCTION_RECTANGLE	= UNIT_COORDINATES + " " + KEYWORD_DIMENSION + " " + UNIT_DIMENSIONS;
		String SUITE_INSTRUCTION_RRECTANGLE	= UNIT_COORDINATES + " " + KEYWORD_DIMENSION + " " + UNIT_DIMENSIONS;
		String SUITE_INSTRUCTION_TEXT		= UNIT_COORDINATES + " " + UNIT_TEXT;
		String SUITE_INSTRUCTION_STYLE		= UNIT_STYLE + " " + UNIT_COLOR;
		String SUITE_INSTRUCTION_THICKNESS	= UNIT_EXPRESSION;
		String SUITE_INSTRUCTION_XOR		= UNIT_XOR;
		String SUITE_INSTRUCTION_TRIANGLE	= UNIT_COORDINATES + " " + KEYWORD_AND + " " + UNIT_COORDINATES + " " + KEYWORD_AND +  " " + UNIT_COORDINATES;

		jemmaLanguage.USAGE_INSTRUCTION	= "\n" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(jemma.language) ?
				"Program instructions can be:\n\n" +
				"\t\uFFFA" + KEYWORD_ASSIGN 	+ " " + SUITE_INSTRUCTION_ASSIGN + "\uFFFB\n\t\t\tPut the result of the expression in the named variable.\n\n" +
				"\t\uFFFA" + KEYWORD_DO 		+ " " + SUITE_INSTRUCTION_DO + "\uFFFB\n\t\t\tRepeat the following instructions until "+ KEYWORD_ENDDO + " n times, n being the result of the expression.\n\n" +
				"\t\uFFFA" + KEYWORD_ENDDO 		+ " " + SUITE_INSTRUCTION_ENDDO + "\uFFFB\n\t\t\tThis instruction delimits the end of the sequence to repeat.\n\n" +
				"\t\uFFFA" + KEYWORD_ARC 		+ " " + SUITE_INSTRUCTION_ARC + "\uFFFB\n\t\t\tAn arc is drawn within the area bounded by the rectangle whose top left corner is x,y and has the specified dimension: \"width\" and \"height\". The start angle \"startAngle\" is on the horizontal x axis, so 0 degrees does not represent North but instead can be thought of as pointing East.  The arc is drawn anticlockwise for the number of degrees specified by \"endAngle\".\n\n" +
				"\t\uFFFA" + KEYWORD_LINE 		+ " " + SUITE_INSTRUCTION_LINE + "\uFFFB\n\t\t\tDraw a line between two points, beginning at x1, y1 and ending at x2, y2.\n\n" +
				"\t\uFFFA" + KEYWORD_OVAL 		+ " " + SUITE_INSTRUCTION_OVAL + "\uFFFB\n\t\t\tAn oval is draw within the rectangle defined by its top left corner (x,y) and its width and height. To draw a circle set the width and height to be the same value.\n\n" +
				"\t\uFFFA" + KEYWORD_RECTANGLE 	+ " " + SUITE_INSTRUCTION_RECTANGLE + "\uFFFB\n\t\t\tDraw a rectangle with the top left corner at x,y with the specified width and height.\n\n" +
				"\t\uFFFA" + KEYWORD_RRECTANGLE + " " + SUITE_INSTRUCTION_RRECTANGLE + "\uFFFB\n\t\t\tA rounded rectangle differs from a standard rectangle in that its corners are rounded.\n\n" +
				"\t\uFFFA" + KEYWORD_TRIANGLE 	+ " " + SUITE_INSTRUCTION_TRIANGLE + "\uFFFB\n\t\t\tDraw a triangle with its three vertices, clockwise.\n\n" +
				"\t\uFFFA" + KEYWORD_TEXT 		+ " " + SUITE_INSTRUCTION_TEXT + "\uFFFB\n\t\t\tTo draw text you define its top left corner.\n\n" +
				"\t\uFFFA" + KEYWORD_FILL 		+ " " + SUITE_INSTRUCTION_FILL + "\uFFFB\n\t\t\tShapes (oval, rectangle, etc.) can be filled with the defined color (see the \"Colors\" page). The word \"NO\" removes the filling.\n\n" +
				"\t\uFFFA" + KEYWORD_STYLE 		+ " " + SUITE_INSTRUCTION_STYLE + "\uFFFB\n\t\t\tLines can be drawn in a number of styles that are predefined (SOLID, DOT, DASH) in a number of colors (see the \"Colors\" page).\n\n" +
				"\t\uFFFA" + KEYWORD_THICKNESS 	+ " " + SUITE_INSTRUCTION_THICKNESS + "\uFFFB\n\t\t\tModify the thickness of the line which is by default 1.\n\n" +
				"\t\uFFFA" + KEYWORD_XOR 		+ " " + SUITE_INSTRUCTION_XOR + "\uFFFB\n\t\t\tWhen a drawing is drawn, its strokes and its content if it is in fill mode, overwrites the drawing or the drawing part found in the same location. If the XOR mode is set (mode = \"YES\"), the red, green and blue colors of the new drawing are combined with the existing red, green and blue colors and the result gives the new color.\n\n" :
		"Les instructions d'un programme peuvent être :\n\n" +
				"\t\uFFFA" + KEYWORD_ASSIGN 	+ " " + SUITE_INSTRUCTION_ASSIGN + "\uFFFB\n\t\t\tMettre le résultat de l'expression dans la variable nommée.\n\n" +
				"\t\uFFFA" + KEYWORD_DO 		+ " " + SUITE_INSTRUCTION_DO + "\uFFFB\n\t\t\tRépéter les instructions qui suivent jusqu'à \uFFFA" + KEYWORD_ENDDO + "\uFFFB n fois, n étant le résultat de l'expression.\n\n" +
				"\t\uFFFA" + KEYWORD_ENDDO		+ " " + SUITE_INSTRUCTION_ENDDO + "\uFFFB\n\t\t\tCette instruction délimite la fin de la séquence à répéter.\n\n" +
				"\t\uFFFA" + KEYWORD_ARC 		+ " " + SUITE_INSTRUCTION_ARC + "\uFFFB\n\t\t\tUn arc est dessiné dans la zone délimitée par le rectangle dont le coin supérieur gauche est x, y et a les dimensions spécifiées : \"largeur\" et \"hauteur\". L' angle de départ \"angleDépart\" est sur l'axe x horizontal, donc 0 degré ne représente pas le Nord mais peut être considéré comme pointant vers l'Est. L'arc est dessiné dans le sens inverse des aiguilles d'une montre pour le nombre de degrés spécifié par l'angle d'arrivée \"angleArrivée\".\n\n" +
				"\t\uFFFA" + KEYWORD_LINE 		+ " " + SUITE_INSTRUCTION_LINE + "\uFFFB\n\t\t\tTracer une ligne entre deux points, commençant à x1, y1 et se terminant à x2, y2.\n\n" +
				"\t\uFFFA" + KEYWORD_OVAL 		+ " " + SUITE_INSTRUCTION_OVAL + "\uFFFB\n\t\t\tUn ovale est dessiné dans le rectangle défini par son coin supérieur gauche (x, y) et sa largeur  et sa hauteur . Pour dessiner un cercle, définissez la largeur et la hauteur sur la même valeur.\n\n" +
				"\t\uFFFA" + KEYWORD_RECTANGLE 	+ " " + SUITE_INSTRUCTION_RECTANGLE + "\uFFFB\n\t\t\tDessiner un rectangle avec le coin supérieur gauche à x, y avec la largeur et la hauteur.\n\n" +
				"\t\uFFFA" + KEYWORD_RRECTANGLE + " " + SUITE_INSTRUCTION_RRECTANGLE + "\uFFFB\n\t\t\tC'est est un rectangle arrondi qui diffère d'un rectangle standard en ce que ses coins sont arrondis.\n\n" +
				"\t\uFFFA" + KEYWORD_TRIANGLE 	+ " " + SUITE_INSTRUCTION_TRIANGLE + "\uFFFB\n\t\t\tDessiner un triangle avec ses trois sommets, dans le sens des aiguilles d'une montre.\n\n" +
				"\t\uFFFA" + KEYWORD_TEXT 		+ " " + SUITE_INSTRUCTION_TEXT + "\uFFFB\n\t\t\tPour dessiner du texte, vous définissez son coin supérieur gauche (x, y).\n\n" +
				"\t\uFFFA" + KEYWORD_FILL 		+ " " + SUITE_INSTRUCTION_FILL + "\uFFFB\n\t\t\tLes formes (ovales, rectangulaires, etc.) peuvent être remplies avec la couleur définie (voir la page \"Couleurs\"). Le mot \"NON\" supprime le remplissage.\n\n" +
				"\t\uFFFA" + KEYWORD_STYLE 		+ " " + SUITE_INSTRUCTION_STYLE + "\uFFFB\n\t\t\tLes lignes peuvent être dessinées dans un certain nombre de styles prédéfinis (PLEIN, POINT, TIRET) et dans un certain nombre de couleurs (voir la page \"Couleur\").\n\n" +
				"\t\uFFFA" + KEYWORD_THICKNESS 	+ " " + SUITE_INSTRUCTION_THICKNESS + "\uFFFB\n\t\t\tModifier l'épaisseur du trait qui est par défaut 1.\n\n" +
				"\t\uFFFA" + KEYWORD_XOR 		+ " " + SUITE_INSTRUCTION_XOR + "\uFFFB\n\t\t\tLorsque qu'un dessin est tracé, ses traits et son contenu s'il est en mode remplissage, écrase le dessin ou la partie de dessin se trouvant au même emplacement. Si le mode XOR est positionné (mode = \"OUI\"), les couleurs rouges, vertes et bleues du nouveau dessin sont combinées avec les couleurs rouges, vertes et bleues existantes et le résultat donne la nouvelle couleur.\n\n");

		jemmaLanguage.instructionTab = new jemmaLanguage.instructionClass[] {
				new jemmaLanguage.instructionClass(KEYWORD_ARC,			SUITE_INSTRUCTION_ARC,			jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_ASSIGN,		SUITE_INSTRUCTION_ASSIGN,		jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_DO,			SUITE_INSTRUCTION_DO,			jemmaLanguage.instructionClass.eIndentation.RIGHT),
				new jemmaLanguage.instructionClass(KEYWORD_ENDDO,		SUITE_INSTRUCTION_ENDDO,		jemmaLanguage.instructionClass.eIndentation.LEFT),
				new jemmaLanguage.instructionClass(KEYWORD_FILL,		SUITE_INSTRUCTION_FILL,			jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_LINE,		SUITE_INSTRUCTION_LINE,			jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_OVAL,		SUITE_INSTRUCTION_OVAL,			jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_RECTANGLE,	SUITE_INSTRUCTION_RECTANGLE,	jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_RRECTANGLE,	SUITE_INSTRUCTION_RRECTANGLE,	jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_STYLE,		SUITE_INSTRUCTION_STYLE,		jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_TEXT,		SUITE_INSTRUCTION_TEXT,			jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_THICKNESS,	SUITE_INSTRUCTION_THICKNESS,	jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_TRIANGLE,	SUITE_INSTRUCTION_TRIANGLE,		jemmaLanguage.instructionClass.eIndentation.NONE),
				new jemmaLanguage.instructionClass(KEYWORD_XOR,			SUITE_INSTRUCTION_XOR,			jemmaLanguage.instructionClass.eIndentation.NONE)
		};

		jemmaLanguage.unitTIPS = new jemmaLanguage.tips[] {
				new jemmaLanguage.tips(UNIT_COORDINATES, (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Each point in the drawing area is identified by two numbers which are the coordinates of the point. The first number is always the abscissa and the second is the ordinate. The abscissa is often named \\ \"x \\\" (or x1, x2, ...) and the ordinate \\ \"y \\\" (or y1, y2, ...).\n" +
								"The x-axis is the horizontal line at the top of the drawing area.\n" +
								"The ordinate axis is the vertical line to the left of the drawing area.\n" +
								"If the drawing area has a width of 500 and a height of 400:\n" +
								"	- the point in the upper left corner of the drawing area has the coordinates: 0, 0.\n" +
								"	- the point in the bottom right corner of the drawing area has the coordinates: 500, 400.\n" +
								"	- the point in the top right corner of the drawing area has the coordinates: 500, 0.\n" +
								"	- the point in the bottom left corner of the drawing area has the coordinates: 0, 400." :
						"Chaque point de la zone de dessin est repéré par deux nombres qui sont les coordonnées du point. Le premier nombre est toujours l'abscisse et le second est l'ordonnée. L'abscisse est souvent nommé \\\"x\\\" (ou x1, x2, ...) et l'ordonnée \\\"y\\\" (ou y1, y2, ...).\n" +
								"L'axe des abscisses est la ligne horizontale en haut de la zone de dessin.\n" +
								"L'axe des ordonnées est la ligne verticale à gauche de la zone de dessin.\n" +
								"Si la zone de dessin a une largeur de 500 et une hauteur de  400 :\n" +
								"	- le point dans le coin en haut et à gauche de la zone de dessin a les coordonnées: 0, 0.\n" +
								"	- le point dans le coin en bas et à droite  de la zone de dessin a les coordonnées: 500, 400.\n" +
								"	- le point dans le coin en haut et à droite de la zone de dessin a les coordonnées: 500, 0.\n" +
								"	- le point dans le coin en bas et à gauche  de la zone de dessin a les coordonnées: 0, 400.\n")),
				new jemmaLanguage.tips(UNIT_COLOR, (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Available colours are: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE and GRAY.\n" :
						"Les couleurs disponibles sont : NOIR, ROUGE, VERT, JAUNE, BLEU, MAGENTA, CYAN, BLANC et GRIS.")),
				new jemmaLanguage.tips(UNIT_EXPRESSION, (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"An expression is a combination of following operations, may be between parenthesis. An operand is a variable or a number.\n\n" +
								"Addition:			operand + operand\n" +
								"Substraction:		operand - operand\n" +
								"Multiplication:		operand * operand\n" +
								"Division:			operand / operand\n\n" +
								"Examples: N + 3 or (N + 3) * 2" :
						"Une expression est une combinaison des opérations suivantes, peut-être entre parenthèses. Un opérande est une variable ou un nombre.\n\n" +
								"Addition:			opérande + opérande\n" +
								"Soustraction:		opérande - opérande\n" +
								"Multiplication:		opérande * opérande\n" +
								"Division:			opérande / opérande\n\n" +
								"Exemples : N + 3 ou (N + 3) * 2")),
				new jemmaLanguage.tips(UNIT_VARIABLE,	(jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Variables are symbols that associate a name (identifier) with a value. The name is unique but the value may change over time. For the name of a variable:\n" +
								"	- there can only be lowercase, uppercase and numbers;\n" +
								"	- the name of the variable must start with a letter;\n" +
								"	- spaces are prohibited. Instead, you can use the underscore character _;\n" +
								"	- you are not allowed to use accents.\n" :
						"Les variables sont des symboles qui associent un nom (l'identifiant) à une valeur. Le nom est unique mais la valeur peut changer au cours du temps. Pour le nom d'une variable :\n" +
								"	- il ne peut y avoir que des minuscules, majuscules et des chiffres;\n" +
								"	- le nom de la variable doit commencer par une lettre;\n" +
								"	- les espaces sont interdits. À la place, on peut utiliser le caractère « underscore » _;\n" +
								"	- tu n'as pas le droit d'utiliser des accents.\n")),
				new jemmaLanguage.tips(UNIT_STYLE, (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Lines can be drawn in a number of predefined styles (SOLID, DOT, DASH)" :
						"Les lignes peuvent être dessinées dans un certain nombre de styles prédéfinis (PLEIN, POINT, TIRET)")),
				new jemmaLanguage.tips(UNIT_FILLING, (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Shapes (oval, rectangle, etc.) can be filled with the defined color. The word \"NO\" removes the filling." :
						"Les formes (ovales, rectangulaires, etc.) peuvent être remplies avec la couleur définie. Le mot \"NON\" supprime le remplissage.")),
				new jemmaLanguage.tips(UNIT_XOR, (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"By default, the XOR mode is not set (mode = \"NO\"). So when a drawing is drawn, its strokes and its content if it is in fill mode, overwrites the drawing or the drawing part found in the same location.\n" +
						"When the XOR mode is set (mode = \"YES\"), the red, green and blue colors of the new drawing are combined with the existing red, green and blue colors and the result gives the new color." :
						"Par défaut, le mode XOR n'est pas  positionné (mode = \"NON\"). Donc, lorsque qu'un dessin est tracé, ses traits et son contenu s'il est en mode remplissage, écrase le dessin ou la partie de dessin se trouvant au même emplacement.\n" +
						"Quand le mode XOR est positionné (mode = \"OUI\"), les couleurs rouges, vertes et bleues du nouveau dessin sont combinées avec les couleurs rouges, vertes et bleues existantes et le résultat donne la nouvelle couleur.")),
				new jemmaLanguage.tips(UNIT_TEXT, (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Text to write. All characters, until the end of the instruction, are accepted." :
						"Texte à écrire. Tous les caractères, jusqu'à la fin de l'instruction, sont acceptés.")),
				new jemmaLanguage.tips(UNIT_DIMENSIONS, (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"Horizontal (width) and vertical (height) dimension of the shape (oval, rectangular, etc.)." :
						"Dimension horizontale (largeur) et verticale (hauteur) de la forme (ovale, rectangulaire, etc.)")),
				new jemmaLanguage.tips(UNIT_ANGLES, (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
						"An arc is drawn within the area bounded by the rectangle whose top left corner is x,y and has the specified dimension: \"width\" and \"height\". The start angle \"startAngle\" is on the horizontal x axis, so 0 degrees does not represent North but instead can be thought of as pointing East.  The arc is drawn anticlockwise for the number of degrees specified by \"endAngle\"." :
						"Un arc est dessiné dans la zone délimitée par le rectangle dont le coin supérieur gauche est x, y et a les dimensions spécifiées : \"largeur\" et \"hauteur\". L' angle de départ \"angleDépart\" est sur l'axe x horizontal, donc 0 degré ne représente pas le Nord mais peut être considéré comme pointant vers l'Est. L'arc est dessiné dans le sens inverse des aiguilles d'une montre pour le nombre de degrés spécifié par l'angle d'arrivée \"angleArrivée\".")),
		};

		jemmaLanguage.USAGE_FIRST		= "\n" + (jemmaLanguage.LANGUAGE_ENGLISH.equals(language) ?
				"To enter a program, click with the mouse in the green window on the left, under the icon.\n\nThen start typing the instruction you want to use. At any time, you can hit the \uFFFATAB\uFFFB key; \uFFFAJEMMA\uFFFB will then complete the instruction that you have started. You will have to adapt the instruction to your needs by selecting with the mouse the fields to define, for example \uFFFA[expression]\uFFFB, \uFFFA[variable]\uFFFB, \uFFFA[width,height]\uFFFB or \uFFFA[x,y]\uFFFB, to replace them with your values ​​(for example \uFFFA30\uFFFB, \uFFFAA\uFFFB, \uFFFA100,200\uFFFB or \uFFFA150,450\uFFFB. To do so, select it by double-clicking with the left mouse button. If you leave the mouse for a few parts of seconds on a field to be defined, help will be displayed on the screen.\n\nThe \uFFFATAB\uFFFB key which allows you to complete the instruction you have started entering, also allows you to display all of the \uFFFAJEMMA\uFFFB commands if you use it at the start of the line. If several commands start with the same letter or letters, then the \uFFFATAB\uFFFB key is also used to display them.\n\nWhen you wish, you can click on the \uFFFACHECK\uFFFB button; \uFFFAJEMMA\uFFFB will then check that what you have written is correct. The errors will be displayed in the console. The console is the green window, bottom left.\n\nAlways when you wish, you can click on the \uFFFAEXECUTE\uFFFB button. You will then see the result of your creation in the window on the right. It may not be the expected result .... because you may have been wrong!!!\n\nGood game..." :
				"Pour entrer un programme, clique avec la souris dans la fenêtre verte à gauche, sous l'icône.\n\nPuis commence à taper l'instruction que tu veux utiliser. A tout moment, tu peux taper sur la touche \uFFFATAB\uFFFB; \uFFFAJEMMA\uFFFB complètera alors l'instruction que tu as commencé. Il te restera à adapter l'instruction à tes besoins en sélectionnant avec la souris les champs à définir, par example \uFFFA[expression]\uFFFB, \uFFFA[variable]\uFFFB, \uFFFA[largeur,hauteur]\uFFFB ou \uFFFA[x,y]\uFFFB, pour les remplacer par tes valeurs (par exemple \uFFFA30\uFFFB, \uFFFAA\uFFFB, \uFFFA100,200\uFFFB ou \uFFFA150,450\uFFFB. Pour cela, sélectionne-le, en double-cliquant avec la touche gauche de la souris. Si tu laisses la souris quelques pouièmes de secondes sur un champs à définir, une aide sera alors afficher à l'écran.\n\nLa touche \uFFFATAB\uFFFB qui te permet de compléter l'instruction que tu as commencé à entrer, te permet aussi à afficher l'ensemble des commandes de \uFFFAJEMMA\uFFFB si tu l'utilises en début de ligne. Si plusieurs commandes commencent pat la ou les mêmes lettres, alors la touche \uFFFATAB\uFFFB permet aussi de les afficher.\n\nQuand tu le souhaites, tu peux cliquer sur le bouton \uFFFAVERIFIER\uFFFB; \uFFFAJEMMA\uFFFB contrôlera alors que ce que tu as écrit est correct. Les erreurs seront affichées dans la console. La console est la fenêtre verte, en bas à gauche.\n\nToujours quand tu le souhaiteras, tu pourras cliquer sur le bouton \uFFFAEXECUTER\uFFFB. Tu verras alors le résultat de ta création dans la fénêtre de droite. Ce ne sera peut être pas le résultat attendu.... car tu t'es peu-être trompé !!!\n\nBon jeu...");
	}
}
