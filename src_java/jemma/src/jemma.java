//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.apache.commons.cli.*;

public class jemma {
	static final String jemmaName						= "jemma";
	static String language 								= jemmaLanguage.LANGUAGE_FRENCH;
	static jemmaLanguage.eLanguage ProgrammingLanguage	= jemmaLanguage.eLanguage.VERB;
	static String installRootDir;
	static String ExampleDir							= "examples";
	static String programDirectory						= System.getProperty("user.dir");
	static String programName							= "";
	static String suffixLanguage						= "";
	static String Prompt								= "JEMMA> ";
	static boolean gridDisplayed 						= false;
	static char startBeautifying						= '\uFFFA';
	static char endBeautifying							= '\uFFFB';

	private static final String jarFile					= "jemma.jar";
	private static final String intelliJobjpath			= "src_java/jemma/out/production/jemma/";
	private static final String sTitle					= jemmaName + " - JAVA application to teach young children the basics of programming";
	private static final String sError 					= "!!! ERROR !!! ";
	private static final String slash 					= System.getProperty("file.separator");

	public static void main(String [] args) {
		installRootDir = jemma.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace(jarFile, "");
		if (installRootDir.length() > intelliJobjpath.length()) {
			if (intelliJobjpath.equals(installRootDir.substring(installRootDir.length() - intelliJobjpath.length()))) {
				installRootDir = installRootDir.substring(0, installRootDir.length() - intelliJobjpath.length() - 1) + slash;
			}
		}
		//----------------------------------------------------------------------
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("h", "help",   false, "print this message and exit");
		options.addOption("V", "version",false, "print the version information and exit");
		options.addOption("F", "french", false, "dialogue in french language (default)");
		options.addOption("E", "english", false, "dialogue in english language");
		options.addOption("v", "verb", false, "programming language is VERB (default)");
		options.addOption("g", "geometry", false, "programming language is GEOMETRY");
		try {
			CommandLine line = parser.parse(options, args);
			// Option: help
			if (line.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(sTitle, options);
				System.out.println();
				Exit(0);
			}
			// Option: version
			if (line.hasOption("version")) {
				System.out.println(jemmaName +
						"	Version: " + jemmaBuildInfo.getVersion() +
						" - Build: " + jemmaBuildInfo.getBuildNumber() +
						" - Date: " + jemmaBuildInfo.getBuildDate());
				Exit(0);
			}
			// Option: english
			if (line.hasOption("english"))
				language = jemmaLanguage.LANGUAGE_ENGLISH;
			// Option: french
			if (line.hasOption("french"))
				language = jemmaLanguage.LANGUAGE_FRENCH;
			// Option: Geometry
			if (line.hasOption("geometry"))
				ProgrammingLanguage = jemmaLanguage.eLanguage.GEOMETRY;
			// Option: Verb
			if (line.hasOption("verb"))
				ProgrammingLanguage = jemmaLanguage.eLanguage.VERB;
		} catch (ParseException exp) { Error("Parsing failed - " + exp.getMessage()); }
		//----------------------------------------------------------------------
		jemmaLanguage.init_language(language);
		if (ProgrammingLanguage == jemmaLanguage.eLanguage.VERB)
			jemmaLanguage_Verb.init_language(language);
		else
			jemmaLanguage_Geometry.init_language(language);
		jemmaUI.screen();
	}
	//--------------------------------------------------------------------------
	private static void Error(String s) {
		System.err.println(sError + s);
		Exit(-1);
	}
	//--------------------------------------------------------------------------
	static void Exit( int n ) {
		jemmaEditor.exit();
		jemmaConsole.exit();
		jemmaUI.exit();
		System.exit(n);
	}
	//--------------------------------------------------------------------------
}
