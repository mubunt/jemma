//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GlyphMetrics;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.ToolTip;

import java.util.ArrayList;

public class jemmaEditor {
	private static final String TAB = "\t";
	private static final String headingString = "\t";
	private static final String beepSound = "beep.mp3";

	private static final Color gray = Display.getDefault().getSystemColor(SWT.COLOR_GRAY);
	private static final Color myDarkGreen = new Color(Display.getDefault(), 0, 41, 0);
	private static final Color myLightGreen = new Color(Display.getDefault(), 0, 230, 0);

	private static final Font editorFont = new Font(Display.getCurrent(), "Courier New", 10, SWT.BOLD);

	private static jemmaEditorOverlay completness;
	private static final Color editorBackground = myDarkGreen;
	private static final Color editorForeground = myLightGreen;

	private static jemmaLanguage.instructionClass[] refInstructions;
	private static boolean editorModified;
	//--------------------------------------------------------------------------
	static StyledText getEditor(Composite parent, jemmaLanguage.instructionClass[] instructions ) {
		StyledText text = new StyledText(parent, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		text.setBackground(editorBackground);
		text.setForeground(editorForeground);
		text.setFont(editorFont);
		text.addLineStyleListener(jemmaEditor::lineNumbering);
		text.addModifyListener(event -> text.redraw());
		text.addModifyListener(event -> editorModified = true);
		text.addVerifyListener(jemmaEditor::lineAnalyzer);
		text.addLineStyleListener(jemmaEditor::lineColoring);
		text.addListener(SWT.MouseUp, jemmaEditor::lineSelection);
		refInstructions = instructions;
		editorModified = false;
		return text;
	}
	//--------------------------------------------------------------------------
	static boolean isModified() {
		return editorModified;
	}
	//--------------------------------------------------------------------------
	static void exit() {
		editorFont.dispose();
		myDarkGreen.dispose();
		myLightGreen.dispose();
	}
	//==========================================================================
	private static void lineNumbering( LineStyleEvent event ) {
		StyledText editor = (StyledText) event.widget;
		StyleRange styleRange = new StyleRange();
		styleRange.foreground = gray;
		int maxLine = editor.getLineCount();
		int bulletLength = Integer.toString(maxLine).length();
		int bulletWidth = (bulletLength + 1) * editor.getLineHeight() / 2;
		styleRange.metrics = new GlyphMetrics(0, 0, bulletWidth);
		event.bullet = new Bullet(ST.BULLET_TEXT, styleRange);
		int bulletLine = editor.getLineAtOffset(event.lineOffset) + 1;
		event.bullet.text = String.format("%" + bulletLength + "s", bulletLine);
	}
	//--------------------------------------------------------------------------
	private static void lineAnalyzer( VerifyEvent event ) {
		if (completness != null && completness.isShowing()) removePanel();
		if (! event.text.equals(TAB) && !event.text.equals("\n")) return;
		event.doit = false;
		StyledText editor = (StyledText) event.widget;
		// Get line number.
		int lineNumber = editor.getLineAtOffset(event.start);
		// Get offset of beginning of the line.
		int lineOffset =  editor.getOffsetAtLine(lineNumber);
		if (event.start == lineOffset) {
			// No word, so display all of instructions
			displayPanel(editor, refInstructions);
			return;
		}
		// Get the word to be completed.
		String line = editor.getText(lineOffset, event.start - 1);
		int startWordOffset = removeWhitespaces(line);
		if (event.start == startWordOffset) { // No word
			if (event.text.equals(TAB)) // so display all of instructions
				displayPanel(editor, refInstructions);
			return;
		}
		String word = takeAWord(line, startWordOffset).toUpperCase();
		// Word recognition.
		jemmaLanguage.instructionClass[] recognized = foundHowManyWords(refInstructions, word);
		if (event.text.equals("\n")) {
			if (recognized.length == 1) {    // If there is only one match
				String s = lineCompletion(
						line.substring(startWordOffset),
						recognized[0].keywordInstruction + " " + recognized[0].suiteInstruction, false);
				editor.replaceTextRange(lineOffset, line.length(), s);
			} else {
				editor.replaceTextRange(lineOffset, line.length(), line + "\n");
			}
			reindentProgram(editor);
		} else {
			switch (recognized.length) {
				case 0:        // If there is no match, beep
					beep();
					break;
				case 1:        // If there is only one match, check the following before displaying the instruction complement.
					if (word.length() < recognized[0].keywordInstruction.length()) {
						// Incomplete keyword, so replace it by all instruction.
						editor.replaceTextRange(lineOffset + startWordOffset, word.length(),
								recognized[0].keywordInstruction + " " + recognized[0].suiteInstruction + jemmaUI.EOL);
					} else {
						// Complete keyword and may be a part of the instruction. SO, complete the rest.
						String s = lineCompletion(
								line.substring(startWordOffset),
								recognized[0].keywordInstruction + " " + recognized[0].suiteInstruction, true);
						editor.replaceTextRange(lineOffset + startWordOffset, line.length(), s);
					}
					reindentProgram(editor);
					break;
				default:    // There are several matches; display all matches.
					displayPanel(editor, recognized);
					break;
			}
		}
	}
	//--------------------------------------------------------------------------
	private static String lineCompletion( String userLine, String templateLine, boolean completeWithTemplate ) {
		String templateString = templateLine.replaceAll(", ", ",").replaceAll(" ,", ",").replaceAll("( )+", " ").trim();
		String[] templateTab = templateString.split(" ");
		StringBuilder templateSB = new StringBuilder(templateString);

		String userString = userLine.replaceAll(", ", ",").replaceAll(" ,", ",").replaceAll("( )+", " ").trim();
		String[] userTab = userString.split(" ");
		StringBuilder userSB = new StringBuilder(userString);

		StringBuilder res = new StringBuilder();
		int n = Math.min(userTab.length, templateTab.length);
		int j;
		for (int i = 0; i < n; i++) {
			j = templateSB.indexOf(templateTab[i]);
			templateSB.delete(j, j + templateTab[i].length());
			j = userSB.indexOf(userTab[i]);
			userSB.delete(j, j + userTab[i].length());
			if (templateTab[i].equals(userTab[i].toUpperCase())) {
				res.append(templateTab[i]);
			} else {
				if (templateTab[i].charAt(0) == '[' && templateTab[i].charAt(templateTab[i].length() - 1) == ']') {
					j = templateTab[i].indexOf(',');
					if (j == -1)
						res.append(userTab[i]);
					else {
						int k;
						for (k = 0; k < userTab[i].length(); k++)
							if (userTab[i].charAt(k) == ',') break;
						res.append(userTab[i]);
						if (k == userTab[i].length())
							res.append(templateTab[i], j, templateTab[i].length() - 1);
						else {
							k++;
							if (k == userTab[i].length())
								res.append(templateTab[i], j + 1, templateTab[i].length() - 1);
						}
					}
				} else {
					//if (userTab[i].toUpperCase().equals(templateTab[i].substring(0, userTab[i].length())) && i == n - 1) {
					if (userTab[i].toUpperCase().equals(templateTab[i])) {
						res.append(templateTab[i]);
					} else {
						res.append(userTab[i]);
					}
				}
			}
			res.append(" ");
		}
		if (n < templateTab.length) {
			if (completeWithTemplate) {
				j = templateSB.indexOf(templateTab[n]);
				char c = ' ';
				if (j > 0) c = templateSB.charAt(j - 1);
				res.append(c).append(templateSB.substring(j));
			}
		} else {
			if (n < userTab.length) {
				j = userSB.indexOf(userTab[n]);
				char c = ' ';
				if (j > 0) c = userSB.charAt(j - 1);
				res.append(c).append(userSB.substring(j));
			}
		}
		return res.toString().replaceAll("( )+", " ").trim() + jemmaUI.EOL;
	}
	//--------------------------------------------------------------------------
	private static void lineColoring(LineStyleEvent event) {
		ArrayList<StyleRange> styles = new ArrayList<>();
		int offset = 0;
		while (offset < event.lineText.length()) {
			StringBuilder word = new StringBuilder();
			if (event.lineText.charAt(offset) != ' ') {
				while (offset < event.lineText.length() && event.lineText.charAt(offset) != ' ') {
					word.append(event.lineText.charAt(offset));
					offset++;
				}
				if (word.length() != 0) {
					if (word.charAt(0) == '[' && word.charAt(word.length() - 1) == ']')
						styles.add(new StyleRange(event.lineOffset + offset - word.length(), word.length(), editorBackground, gray, SWT.NORMAL));
				}
			}
			offset++;
		}
		event.styles = styles.toArray(new StyleRange[0]);
	}
	//--------------------------------------------------------------------------
	private static void lineSelection(Event event) {
		StyledText text = (StyledText) event.widget;
		String selection = text.getSelectionText();
		if (selection.length() == 0) return;
		String content = text.getText();
		Point pselection = text.getSelection();
		if (pselection.x == 0 || pselection.y >= content.length()) return;
		char before = content.charAt(pselection.x - 1);
		char after = content.charAt(pselection.y);
		if (before == '[' && ( after == '|' ||  after == ',')) {
			while (pselection.y < content.length() &&  content.charAt(pselection.y) != ']') pselection.y++;
			if (pselection.y == content.length()) return;
			after = content.charAt(pselection.y);
		}
		if ((before == '|' || before == ',') && after == ']') {
			while (pselection.x >= 0 &&  content.charAt(pselection.x) != '[') pselection.x--;
			if (pselection.x < 0) return;
			before = content.charAt(pselection.x);
			pselection.x++;
		}
		if (before != '[' || after != ']') return;
		selection = text.getText(pselection.x - 1, pselection.y);
		text.setSelection(pselection.x - 1, pselection.y + 1);

		final int TOOLTIP_HIDE_DELAY = 300;	// 0.3sec
		final ToolTip tip = new ToolTip(Display.getCurrent().getActiveShell(), SWT.ICON_INFORMATION);
		for (jemmaLanguage.tips t : jemmaLanguage.unitTIPS) {
			if (t.unit.equals(selection)) {
				tip.setText(selection);
				tip.setMessage(t.tips);
				Point loc = text.toDisplay(text.getLocation());
				tip.setLocation(loc.x + event.x - text.getBorderWidth(), loc.y + event.y);
				tip.setVisible(true);
				text.addListener(SWT.MouseHover, event12 -> { });
				text.addListener(SWT.MouseExit, event1 -> tip.getDisplay().timerExec(TOOLTIP_HIDE_DELAY, () -> tip.setVisible(false)));
				break;
			}
		}
	}
	//--------------------------------------------------------------------------
	private static void reindentProgram( StyledText editor ) {
		int nestedLoops = 0;
		int lineno = 0;
		while (lineno < editor.getLineCount()) {
			String programLine = editor.getLine(lineno);
			if (programLine.length() != 0) {
				int lineoffset =  editor.getOffsetAtLine(lineno);
				int offset = removeWhitespaces(programLine);
				int inst = analyzeInstruction(programLine, offset);
				if (inst != -1) {
					if (refInstructions[inst].indentInstruction == jemmaLanguage.instructionClass.eIndentation.LEFT && nestedLoops > 0)
						--nestedLoops;
				}
				String newline = "";
				if (nestedLoops != 0) newline = new String(new char[nestedLoops]).replace("\0", headingString);
				newline = newline + programLine.substring(offset);
				editor.replaceTextRange(lineoffset, programLine.length(), newline);
				if (inst != -1) {
					if (refInstructions[inst].indentInstruction == jemmaLanguage.instructionClass.eIndentation.RIGHT)
						++nestedLoops;
				}
			}
			++lineno;
		}
	}
	//--------------------------------------------------------------------------
	private static int removeWhitespaces( String programLine ) {
		int offset = 0;
		while (offset < programLine.length() && (programLine.charAt(offset) == ' ' || programLine.charAt(offset) == '\t'))
			++offset;
		return offset;
	}
	//--------------------------------------------------------------------------
	private static int analyzeInstruction( String programLine, int offset) {
		String s = takeAWord(programLine, offset).toUpperCase();
		for (int i = 0; i < refInstructions.length; i++)
			if (s.equals(refInstructions[i].keywordInstruction)) return i;
		return -1;
	}
	//--------------------------------------------------------------------------
	private static String takeAWord( String programLine, int offset ) {
		int index;
		for (index = offset; index < programLine.length(); index++) {
			if (programLine.charAt(index) == ' ') break;
		}
		return programLine.substring(offset, index);
	}
	//--------------------------------------------------------------------------
	private static jemmaEditorOverlay getPanel(Composite parent, int width, int height ) {
		return new jemmaEditorOverlay(parent, width, height);
	}
	private static void removePanel() {
		completness.remove();
		completness = null;
	}
	//--------------------------------------------------------------------------
	private static void displayPanel( StyledText editor, jemmaLanguage.instructionClass[] array ) {
		int maxlength = longestInstructionLength(array);
		// Width of number character is a little bit less than half the height in monospaced font.
		// Add 2-character width for right and left paddings.
		int width = (int) ((maxlength + 2) * editor.getLineHeight() / 2.7);
		int height = (array.length + 2) * editor.getLineHeight();
		completness = getPanel(editor, width, height);
		StringBuilder data = new StringBuilder();
		for (jemmaLanguage.instructionClass s : array) data.append(s.keywordInstruction).append(" ").append(s.suiteInstruction).append("\n");
		completness.setText(data.toString());
		completness.show();
	}
	//--------------------------------------------------------------------------
	private static int longestInstructionLength( jemmaLanguage.instructionClass[] array ) {
		int maxLength = 0;
		for (jemmaLanguage.instructionClass s : array) {
			int n = s.keywordInstruction.length() + s.suiteInstruction.length() + 1;
			if (n > maxLength) maxLength = n;
		}
		return maxLength;
	}
	//--------------------------------------------------------------------------
	private static jemmaLanguage.instructionClass[] foundHowManyWords(jemmaLanguage.instructionClass[] arrayInst, String word ) {
		ArrayList<jemmaLanguage.instructionClass> res = new ArrayList<>();
		for (jemmaLanguage.instructionClass s : arrayInst) {
			if (s.keywordInstruction.length() >= word.length()) {
				String ss = s.keywordInstruction.substring(0, word.length());
				if (ss.equals(word)) res.add(s);
			}
		}
		return res.toArray(new jemmaLanguage.instructionClass[0]);
	}
	//--------------------------------------------------------------------------
	private static void beep() {
		jemmaSounds.playSound(beepSound);
	}
	//--------------------------------------------------------------------------
}
