//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class jemmaExecution_Verb {
	enum actions { DRAW, SKIP }
	enum ways { TONORTH, TOEAST, TOSOUTH, TOWEST }
	private static final String soundCrash 	= "sounds/Car-crash-sound-effect.mp3";


	private static class myLine {
		actions action = actions.DRAW;
		Color foreground = jemmaUI.black;
		int width = 4;
		Point start;
		Point end;
		void setLine(actions a, Point start, Point end, Color c, int pen) {
			this.action = a;
			this.start = start;
			this.end = end;
			this.foreground = c;
			this.width = pen;
			if (jemmaUI.ExecutionArea == null) return;
			if (this.action.equals(actions.DRAW)) {
				GC gc = new GC(jemmaUI.ExecutionArea);
				gc.setForeground(this.foreground);
				gc.setLineWidth(this.width);
				gc.drawLine(this.start.x, this.start.y, this.end.x, this.end.y);
				gc.dispose();
			}
		}
		Point getEnd() { return this.end; }
	}

	private static List<myLine> drawing;
	private static final Point startPoint = new Point(50, 50);
	private static Color currentColor;
	private static ways currentWay;
	private static int currentPen;
	//--------------------------------------------------------------------------
	static boolean analyzer(boolean executionRequested) {
		stepByStepExecutionStart();
		boolean correctness = true;
		boolean execution = executionRequested;
		while (jemmaExecution.lineno < jemmaUI.editor.getLineCount()) {
			jemmaExecution.programLine = jemmaUI.editor.getLine(jemmaExecution.lineno);
			if (jemmaExecution.programLine.length() == 0) {
				++jemmaExecution.lineno;
			} else {
				jemmaExecution.oneInstruction_result res = oneInstruction_analyzer(jemmaExecution.lineno, execution);
				correctness = correctness & res.correctness;
				if (execution && ! correctness) execution = false;
				jemmaExecution.lineno = res.lineno;
			}
		}
		if (jemmaExecution.nestedLoops != 0)
			correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_NESTED_LOOPS);
		if (correctness && ! execution)
			jemmaUI.editor.setText(jemmaExecution.analyzed_program.toString());
		if (executionRequested && ! execution)
			jemmaExecution.displayError(-1, jemmaLanguage.ERROR_EXEC_STOP);
		return correctness;
	}
	//--------------------------------------------------------------------------
	static void stepByStepExecutionStart( ) {
		jemmaExecution.symbolsTable = new ArrayList<>();
		currentColor = jemmaUI.black;
		currentWay = ways.TOEAST;
		currentPen = 4;
		clearDrawing();
		drawing = new ArrayList<>();
		myLine d = new myLine();
		d.setLine(actions.SKIP, startPoint, startPoint, currentColor, currentPen);
		drawing.add(d);
		jemmaExecution.loop = new Stack<>();
		jemmaExecution.analyzed_program = new StringBuilder();
		jemmaExecution.nestedLoops = 0;
		jemmaExecution.lineno = 0;
	}
	static boolean stepByStepExecutionNext( ) {
		boolean correctness = true;
		jemmaExecution.programLine = jemmaUI.editor.getLine(jemmaExecution.lineno);
		if (jemmaExecution.programLine.length() == 0) {
			++jemmaExecution.lineno;
		} else {
			jemmaExecution.oneInstruction_result res = oneInstruction_analyzer(jemmaExecution.lineno, true);
			jemmaExecution.lineno = res.lineno;
			correctness = res.correctness;
		}
		return correctness;
	}
	//--------------------------------------------------------------------------
	private static jemmaExecution.oneInstruction_result oneInstruction_analyzer(int lineno, boolean execution) {
		jemmaExecution.oneInstruction_result result= new jemmaExecution.oneInstruction_result();
		result.correctness = true;
		result.lineno = lineno + 1;
		jemmaExpression.evalExpression expr;
		myLine newdraw, prevdraw;
		jemmaExecution.instdo doloop;

		int inst;
		switch (inst = jemmaExecution.analyzeInstruction()) {
			//--------------------------------------------------------
			case 0:        // KEYWORD_MOVEFORWARD
				expr = jemmaExecution.analyzeExpression(jemmaLanguage_Verb.KEYWORD_STEPS);
				if (! expr.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr.error);
					break;
				}
				if ( ! jemmaLanguage_Verb.KEYWORD_STEPS.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					prevdraw = drawing.get(drawing.size() - 1);
					newdraw = new myLine();
					newdraw.setLine(actions.DRAW, prevdraw.getEnd(), computeEnd(expr.result, prevdraw.getEnd(), currentWay), currentColor, currentPen);
					drawing.add(newdraw);
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr.expression).append(" ")
							.append(jemmaLanguage_Verb.KEYWORD_STEPS).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 1:        // KEYWORD_GOBACKWARD
				expr = jemmaExecution.analyzeExpression(jemmaLanguage_Verb.KEYWORD_STEPS);
				if (! expr.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr.error);
					break;
				}
				if ( ! jemmaLanguage_Verb.KEYWORD_STEPS.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					prevdraw = drawing.get(drawing.size() - 1);
					newdraw = new myLine();
					newdraw.setLine(actions.DRAW, prevdraw.getEnd(), computeEnd(expr.result, prevdraw.getEnd(), oppositeWay(currentWay)), currentColor, currentPen);
					drawing.add(newdraw);
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr.expression).append(" ")
							.append(jemmaLanguage_Verb.KEYWORD_STEPS).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 2:        // KEYWORD_SKIP
				expr = jemmaExecution.analyzeExpression(jemmaLanguage_Verb.KEYWORD_STEPS);
				if (! expr.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr.error);
					break;
				}
				if ( ! jemmaLanguage_Verb.KEYWORD_STEPS.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					prevdraw = drawing.get(drawing.size() - 1);
					newdraw = new myLine();
					newdraw.setLine(actions.SKIP, prevdraw.getEnd(), computeEnd(expr.result, prevdraw.getEnd(), currentWay), currentColor, currentPen);
					drawing.add(newdraw);
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr.expression).append(" ")
							.append(jemmaLanguage_Verb.KEYWORD_STEPS).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 3:        // KEYWORD_TURN
				String direction = analyzeDirection();
				if (direction.length() == 0) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_DIRECTION);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution)
					currentWay = getDirection(currentWay, direction);
				else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(direction).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 4:        // KEYWORD_DO
				expr = jemmaExecution.analyzeExpression(jemmaLanguage_Verb.KEYWORD_TIMES);
				if (! expr.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr.error);
					break;
				}
				if ( ! jemmaLanguage_Verb.KEYWORD_TIMES.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					if (expr.result == 0)
						result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_LOOP_NULL);
					else {
						doloop = new jemmaExecution.instdo();
						doloop.setDo(jemmaExecution.lineno + 1, expr.result);
						jemmaExecution.loop.push(doloop);
					}
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr.expression).append(" ")
							.append(jemmaLanguage_Verb.KEYWORD_TIMES).append(jemmaUI.EOL);
					++jemmaExecution.nestedLoops;
				}
				break;
			//--------------------------------------------------------
			case 5:        // KEYWORD_ENDDO
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					if (jemmaExecution.loop.empty())
						result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_NESTED_LOOPS);
					else {
						doloop = jemmaExecution.loop.pop();
						if (doloop.loopOrNot()) {
							result.lineno = doloop.lineno;
							jemmaExecution.loop.push(doloop);
						}
					}
				} else {
					--jemmaExecution.nestedLoops;
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 6:        // KEYWORD_ASSIGN
				expr = jemmaExecution.analyzeExpression(jemmaLanguage_Verb.KEYWORD_TO);
				if (! expr.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr.error);
					break;
				}
				if ( ! jemmaLanguage_Verb.KEYWORD_TO.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				String variable = jemmaExecution.analyzeVariable(jemmaExecution.symbolsTable);
				if (variable.length() == 0) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_VARIABLE);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					jemmaExecution.symbolsTable_put(jemmaExecution.symbolsTable, variable, expr.result);
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr.expression).append(" ")
							.append(jemmaLanguage_Verb.KEYWORD_TO).append(" ")
							.append(variable).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 7:        // KEYWORD_COLOR
				String s = jemmaExecution.takeAWord().toUpperCase();
				String color = jemmaExecution.analyzeColor(s);
				if (color.length() == 0) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_COLOR);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}

				if (execution) {
					currentColor = jemmaExecution.getSystemColor(color);
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(color).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 8:		// KEYWORD_USE
				expr = jemmaExecution.analyzeExpression("");
				if (! expr.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr.error);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					currentPen = expr.result;
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr.expression).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case -1:    // Unknown
			default:
				result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
				break;
		}
		return result;
	}
	//--------------------------------------------------------------------------
	private static String analyzeDirection( ) {
		String s = jemmaExecution.takeAWord().toUpperCase();
		if (s.equals(jemmaLanguage_Verb.KEYWORD_LEFT) || s.equals(jemmaLanguage_Verb.KEYWORD_RIGHT)) return s;
		return "";
	}
	private static Point computeEnd(int value, Point start, ways way) {
		Point end = new Point(0, 0);
		switch (way) {
			case TOEAST:
				end.x = start.x + value;
				end.y = start.y;
				break;
			case TOSOUTH:
				end.x = start.x;
				end.y = start.y + value;
				break;
			case TOWEST:
				end.x = start.x - value;
				end.y = start.y;
				break;
			case TONORTH:
				end.x = start.x;
				end.y = start.y - value;
				break;
		}
		if (end.x < 0) {
			end.x = 0;
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.ERROR_WALL, jemmaConsole.msgType.ERROR);
			jemmaSounds.playSound(soundCrash);
		}
		Point executionSize = jemmaUI.ExecutionArea.getSize();
		if (end.x >= executionSize.x) {
			end.x = executionSize.x - 1;
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.ERROR_WALL, jemmaConsole.msgType.ERROR);
			jemmaSounds.playSound(soundCrash);
		}
		if (end.y < 0) {
			end.y = 0;
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.ERROR_WALL, jemmaConsole.msgType.ERROR);
			jemmaSounds.playSound(soundCrash);
		}
		if (end.y >= executionSize.y) {
			end.x = executionSize.y - 1;
			jemmaConsole.writeConsole(jemma.Prompt, jemmaLanguage.ERROR_WALL, jemmaConsole.msgType.ERROR);
			jemmaSounds.playSound(soundCrash);
		}
		return end;
	}
	private static ways oppositeWay( ways w ) {
		switch (w) {
			case TOEAST: return ways.TOWEST;
			case TOSOUTH: return ways.TONORTH;
			case TOWEST: return ways.TOEAST;
			case TONORTH: return ways.TOSOUTH;
		}
		return w;
	}
	private static ways getDirection( ways w, String direction ) {
		int i = w.ordinal();
		if (direction.equals(jemmaLanguage_Verb.KEYWORD_RIGHT)) 	// KEYWORD_RIGHT
			i = (i + 1) % 4;
		else
			if (i == 0) i = 3; else i = (i - 1) % 4;
		return ways.values()[i];
	}
	//--------------------------------------------------------------------------
	static void clearDrawing( ) {
		if (drawing == null) return;
		GC gc = new GC(jemmaUI.ExecutionArea);
		gc.setForeground(jemmaUI.executionarea_background);
		for (myLine m : drawing) {
			gc.setLineWidth(m.width);
			if (m.action.equals(actions.DRAW)) gc.drawLine(m.start.x, m.start.y, m.end.x, m.end.y);
		}
		gc.dispose();
		drawing.clear();
	}
	static void Drawing( PaintEvent e ) {
		if (drawing == null) return;
		for (myLine m : drawing) {
			if (m.action.equals(actions.DRAW)) {
				e.gc.setForeground(m.foreground);
				e.gc.setLineWidth(m.width);
				e.gc.drawLine(m.start.x, m.start.y, m.end.x, m.end.y);
			}
		}
	}
}
