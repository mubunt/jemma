//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class jemmaExecution_Geometry {
	enum actions { ARC, LINE, RECTANGLE, ROUNDEDRECTANGLE, OVAL, TEXT, TRIANGLE }

	private static class myshape {					// Line	Rectangle	RRectangle	Oval	Arc	Text	Triangle
		actions action = actions.LINE;				//
		Point start;								// X	X			X			X		X	X		X
		Point end;									// X	-			-			-		-	-		X
		Point intermediate;							// -	-			-			-		-	-		X
		int width;									// -	X			X			X		X	-		-
		int height;									// -	X			X			X		X	-		-
		int lineStyle;								// X	X			X			X		X	-		X
		int thickness;								// X	X			X			X		X	-		X
		int arcWidth;								// -	-			X			-		-	-		-
		int arcHeight;								// -	-			X			-		-	-		-
		int startAngle;								// -	-			-			-		X	-		-
		int endAngle;								// -	-			-			-		X	-		-
		String text;								// -	-			-			-		-	X		-
		Color foreground;							// X	X			X			X		X	X		X
		boolean fill;								// -	X			X			X		X	-		X
		Color background;							// -	X			X			X		X	-		X
		boolean xor;								// X	X			X			X		X	X		X

		void setLine(int x1, int y1, int x2, int y2) {
			this.action = actions.LINE;
			this.start = new Point(x1, y1);
			this.end = new Point(x2, y2);
			this.lineStyle = currentLineStyle;
			this.thickness = currentThickness;
			this.foreground = currentForeground;
			this.xor = currentXor;
		}
		void setRectangle(int x, int y, int width, int height) {
			this.action = actions.RECTANGLE;
			this.start = new Point(x, y);
			this.width = width;
			this.height = height;
			this.lineStyle = currentLineStyle;
			this.thickness = currentThickness;
			this.foreground = currentForeground;
			this.xor = currentXor;
			this.fill = currentFill;
			this.background = currentBackground;
		}
		void setRRectangle(int x, int y, int width, int height) {
			this.action =  actions.ROUNDEDRECTANGLE;
			this.start = new Point(x, y);
			this.width = width;
			this.height = height;
			this.lineStyle = currentLineStyle;
			this.thickness = currentThickness;
			this.foreground = currentForeground;
			this.xor = currentXor;
			this.fill = currentFill;
			this.background = currentBackground;
			this.arcWidth = 25;
			this.arcHeight = 15;
		}
		void setOval(int x, int y, int width, int height) {
			this.action =  actions.OVAL;
			this.start = new Point(x, y);
			this.width = width;
			this.height = height;
			this.lineStyle = currentLineStyle;
			this.thickness = currentThickness;
			this.foreground = currentForeground;
			this.xor = currentXor;
			this.fill = currentFill;
			this.background = currentBackground;
		}
		void setArc(int x, int y, int width, int height, int startangle, int endangle) {
			this.action =  actions.ARC;
			this.start = new Point(x, y);
			this.width = width;
			this.height = height;
			this.startAngle = startangle;
			this.endAngle = endangle;
			this.lineStyle = currentLineStyle;
			this.thickness = currentThickness;
			this.foreground = currentForeground;
			this.xor = currentXor;
			this.fill = currentFill;
			this.background = currentBackground;
		}
		void setText(int x, int y, String text) {
			this.action =  actions.TEXT;
			this.start = new Point(x, y);
			this.text = text;
			this.lineStyle = currentLineStyle;
			this.thickness = currentThickness;
			this.foreground = currentForeground;
			this.xor = currentXor;
		}
		void setTriangle(int x1, int y1, int x2, int y2, int x3, int y3) {
			this.action = actions.TRIANGLE;
			this.start = new Point(x1, y1);
			this.intermediate = new Point(x2, y2);
			this.end = new Point(x3, y3);
			this.lineStyle = currentLineStyle;
			this.thickness = currentThickness;
			this.foreground = currentForeground;
			this.background = currentBackground;
			this.fill =  currentFill;
			this.xor = currentXor;
		}
	}

	private static List<myshape> drawing;
	private static Color currentBackground = jemmaUI.white;
	private static Color currentForeground = jemmaUI.black;
	private static int currentThickness = 4;
	private static boolean currentXor = false;
	private static int currentLineStyle = SWT.LINE_SOLID;
	private static boolean currentFill = false;
	//--------------------------------------------------------------------------
	static boolean analyzer(boolean executionRequested) {
		stepByStepExecutionStart();
		boolean correctness = true;
		boolean execution = executionRequested;
		while (jemmaExecution.lineno < jemmaUI.editor.getLineCount()) {
			jemmaExecution.programLine = jemmaUI.editor.getLine(jemmaExecution.lineno);
			if (jemmaExecution.programLine.length() == 0) {
				++jemmaExecution.lineno;
			} else {
				jemmaExecution.oneInstruction_result res = oneInstruction_analyzer(jemmaExecution.lineno, execution);
				correctness = correctness & res.correctness;
				if (execution && ! correctness) execution = false;
				jemmaExecution.lineno = res.lineno;
			}
		}
		if (jemmaExecution.nestedLoops != 0)
			correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_NESTED_LOOPS);
		if (correctness && ! execution)
			jemmaUI.editor.setText(jemmaExecution.analyzed_program.toString());
		if (executionRequested && ! execution)
			jemmaExecution.displayError(-1, jemmaLanguage.ERROR_EXEC_STOP);
		return correctness;
	}
	//--------------------------------------------------------------------------
	static void stepByStepExecutionStart( ) {
		jemmaExecution.symbolsTable = new ArrayList<>();
		currentBackground = jemmaUI.white;
		currentForeground = jemmaUI.black;
		currentXor = false;
		currentLineStyle = SWT.LINE_SOLID;
		currentThickness = 4;
		currentFill = false;
		clearDrawing();
		drawing = new ArrayList<>();
		jemmaExecution.loop = new Stack<>();
		jemmaExecution.analyzed_program = new StringBuilder();
		jemmaExecution.nestedLoops = 0;
		jemmaExecution.lineno = 0;
	}
	//--------------------------------------------------------------------------
	static boolean stepByStepExecutionNext( ) {
		boolean correctness = true;
		jemmaExecution.programLine = jemmaUI.editor.getLine(jemmaExecution.lineno);
		if (jemmaExecution.programLine.length() == 0) {
			++jemmaExecution.lineno;
		} else {
			jemmaExecution.oneInstruction_result res = oneInstruction_analyzer(jemmaExecution.lineno, true);
			jemmaExecution.lineno = res.lineno;
			correctness = res.correctness;
		}
		return correctness;
	}
	//--------------------------------------------------------------------------
	private static jemmaExecution.oneInstruction_result oneInstruction_analyzer(int lineno, boolean execution ) {
		jemmaExecution.oneInstruction_result result = new jemmaExecution.oneInstruction_result();
		result.correctness = true;
		result.lineno = lineno + 1;
		jemmaExpression.evalExpression expr1, expr2, expr3, expr4, expr5, expr6;
		myshape newdraw;
		jemmaExecution.instdo doloop;
		String s;

		int inst;
		switch (inst = jemmaExecution.analyzeInstruction()) {
			case 0:        // KEYWORD_ARC
				expr1 = jemmaExecution.analyzeExpression(",");
				if (! expr1.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr1.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr2 = jemmaExecution.analyzeExpression(jemmaLanguage_Geometry.KEYWORD_DIMENSION);
				if (! expr2.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr2.error);
					break;
				}
				if (! jemmaLanguage_Geometry.KEYWORD_DIMENSION.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr3 = jemmaExecution.analyzeExpression(",");
				if (! expr3.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr3.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr4 = jemmaExecution.analyzeExpression(jemmaLanguage_Geometry.KEYWORD_ANGLES);
				if (! expr4.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr4.error);
					break;
				}
				if (! jemmaLanguage_Geometry.KEYWORD_ANGLES.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr5 = jemmaExecution.analyzeExpression(",");
				if (! expr5.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr5.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr6 = jemmaExecution.analyzeExpression("");
				if (! expr6.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr6.error);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					newdraw = new myshape();
					newdraw.setArc(expr1.result, expr2.result, expr3.result, expr4.result, expr5.result, expr6.result);
					drawing.add(newdraw);
					GC gc = new GC(jemmaUI.ExecutionArea);
					drawArc(gc, newdraw, false);
					gc.dispose();
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr1.expression).append(",")
							.append(expr2.expression).append(" ")
							.append(jemmaLanguage_Geometry.KEYWORD_DIMENSION).append(" ")
							.append(expr3.expression).append(",")
							.append(expr4.expression).append(" ")
							.append(jemmaLanguage_Geometry.KEYWORD_ANGLES).append(" ")
							.append(expr5.expression).append(",")
							.append(expr6.expression).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 1:        // KEYWORD_ASSIGN
				expr1 = jemmaExecution.analyzeExpression(jemmaLanguage_Geometry.KEYWORD_TO);
				if (! expr1.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr1.error);
					break;
				}
				if (! jemmaLanguage_Geometry.KEYWORD_TO.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				String variable = jemmaExecution.analyzeVariable(jemmaExecution.symbolsTable);
				if (variable.length() == 0) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_VARIABLE);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					jemmaExecution.symbolsTable_put(jemmaExecution.symbolsTable, variable, expr1.result);
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr1.expression).append(" ")
							.append(jemmaLanguage_Geometry.KEYWORD_TO).append(" ")
							.append(variable).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 2:        // KEYWORD_DO
				expr1 = jemmaExecution.analyzeExpression(jemmaLanguage_Geometry.KEYWORD_TIMES);
				if (! expr1.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr1.error);
					break;
				}
				if (! jemmaLanguage_Geometry.KEYWORD_TIMES.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					if (expr1.result == 0)
						result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_LOOP_NULL);
					else {
						doloop = new jemmaExecution.instdo();
						doloop.setDo(jemmaExecution.lineno + 1, expr1.result);
						jemmaExecution.loop.push(doloop);
					}
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr1.expression).append(" ")
							.append(jemmaLanguage_Geometry.KEYWORD_TIMES).append(jemmaUI.EOL);
					++jemmaExecution.nestedLoops;
				}
				break;
			//--------------------------------------------------------
			case 3:        // KEYWORD_ENDDO
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					if (jemmaExecution.loop.empty())
						result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_NESTED_LOOPS);
					else {
						doloop = jemmaExecution.loop.pop();
						if (doloop.loopOrNot()) {
							result.lineno = doloop.lineno;
							jemmaExecution.loop.push(doloop);
						}
					}
				} else {
					--jemmaExecution.nestedLoops;
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 4:        // KEYWORD_FILL
				s = jemmaExecution.takeAWord().toUpperCase();
				String color = jemmaExecution.analyzeColor(s);
				if (color.length() == 0) {
					if ( ! s.equals(jemmaLanguage_Geometry.UNIT_NO)) {
						result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_COLOR);
						break;
					}
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					if (s.equals(jemmaLanguage_Geometry.UNIT_NO))
						currentFill = false;
					else {
						currentFill = true;
						currentBackground = jemmaExecution.getSystemColor(color);
					}
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(s).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 5:        // KEYWORD_LINE
				expr1 = jemmaExecution.analyzeExpression(",");
				if (! expr1.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr1.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr2 = jemmaExecution.analyzeExpression(jemmaLanguage_Geometry.KEYWORD_TO2);
				if (! expr2.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr2.error);
					break;
				}
				if (! jemmaLanguage_Geometry.KEYWORD_TO2.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr3 = jemmaExecution.analyzeExpression(",");
				if (! expr1.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr3.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr4 = jemmaExecution.analyzeExpression("");
				if (! expr2.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr4.error);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					newdraw = new myshape();
					newdraw.setLine(expr1.result, expr2.result, expr3.result, expr4.result);
					drawing.add(newdraw);
					GC gc = new GC(jemmaUI.ExecutionArea);
					drawLine(gc, newdraw, false);
					gc.dispose();
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr1.expression).append(",")
							.append(expr2.expression).append(" ")
							.append(jemmaLanguage_Geometry.KEYWORD_TO2).append(" ")
							.append(expr3.expression).append(",")
							.append(expr4.expression).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 6:        // KEYWORD_OVAL
			case 7:        // KEYWORD_RECTANGLE
			case 8:        // KEYWORD_ROUNDRECTANGLE
				expr1 = jemmaExecution.analyzeExpression(",");
				if (! expr1.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr1.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr2 = jemmaExecution.analyzeExpression(jemmaLanguage_Geometry.KEYWORD_DIMENSION);
				if (! expr2.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr2.error);
					break;
				}
				if (! jemmaLanguage_Geometry.KEYWORD_DIMENSION.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr3 = jemmaExecution.analyzeExpression(",");
				if (! expr3.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr3.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr4 = jemmaExecution.analyzeExpression("");
				if (! expr4.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr4.error);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					newdraw = new myshape();
					GC gc = new GC(jemmaUI.ExecutionArea);
					switch (inst) {
						case 6:        // KEYWORD_OVAL
							newdraw.setOval(expr1.result, expr2.result, expr3.result, expr4.result);
							drawing.add(newdraw);
							drawOval(gc, newdraw, false);
							break;
						case 7:        // KEYWORD_RECTANGLE
							newdraw.setRectangle(expr1.result, expr2.result, expr3.result, expr4.result);
							drawing.add(newdraw);
							drawRectangle(gc, newdraw, false);
							break;
						case 8:        // KEYWORD_ROUNDRECTANGLE
							newdraw.setRRectangle(expr1.result, expr2.result, expr3.result, expr4.result);
							drawing.add(newdraw);
							drawRRectangle(gc, newdraw, false);
							break;
					}
					gc.dispose();
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr1.expression).append(",")
							.append(expr2.expression).append(" ")
							.append(jemmaLanguage_Geometry.KEYWORD_DIMENSION).append(" ")
							.append(expr3.expression).append(",")
							.append(expr4.expression).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 9:        // KEYWORD_STYLE
				int nextLineStyle;
				String s1 = jemmaExecution.takeAWord().toUpperCase();
				if (s1.equals(jemmaLanguage_Geometry.KEYWORD_SOLID)) nextLineStyle = SWT.LINE_SOLID;
				else {
					if (s1.equals(jemmaLanguage_Geometry.KEYWORD_DOT)) nextLineStyle = SWT.LINE_DOT;
					else {
						if (s1.equals(jemmaLanguage_Geometry.KEYWORD_DASH)) nextLineStyle = SWT.LINE_DASH;
						else {
							result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_EXPRESSION);
							break;
						}
					}
				}
				s = jemmaExecution.takeAWord().toUpperCase();
				color = jemmaExecution.analyzeColor(s);
				if (color.length() == 0) {
					if ( ! s.equals(jemmaLanguage_Geometry.UNIT_NO)) {
						result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_COLOR);
						break;
					}
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					currentLineStyle = nextLineStyle;
					currentForeground = jemmaExecution.getSystemColor(color);
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(s1).append(" ")
							.append(color).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 10:        // KEYWORD_TEXT
				expr1 = jemmaExecution.analyzeExpression(",");
				if (! expr1.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr1.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr2 = jemmaExecution.analyzeExpression("\"");
				if (! expr2.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr2.error);
					break;
				}
				String text = jemmaExecution.takeAString();
				if (text.length() == 0) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_INCRCT_INST);
					break;
				}
				if (execution) {
					newdraw = new myshape();
					newdraw.setText(expr1.result, expr2.result, text);
					drawing.add(newdraw);
					GC gc = new GC(jemmaUI.ExecutionArea);
					drawText(gc, newdraw, false);
					gc.dispose();
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr1.expression).append(",")
							.append(expr2.expression).append(" ")
							.append("\"").append(text).append("\"").append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 11:        // KEYWORD_THICKNESS
				expr1 = jemmaExecution.analyzeExpression("");
				if (! expr1.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr1.error);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					currentThickness = expr1.result;
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr1.expression).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 12:			// KEYWORD_TRIANGLE
				expr1 = jemmaExecution.analyzeExpression(",");
				if (! expr1.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr1.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr2 = jemmaExecution.analyzeExpression(jemmaLanguage_Geometry.KEYWORD_AND);
				if (! expr2.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr2.error);
					break;
				}
				if (! jemmaLanguage_Geometry.KEYWORD_AND.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr3 = jemmaExecution.analyzeExpression(",");
				if (! expr3.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr3.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr4 = jemmaExecution.analyzeExpression(jemmaLanguage_Geometry.KEYWORD_AND);
				if (! expr4.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr4.error);
					break;
				}
				if (! jemmaLanguage_Geometry.KEYWORD_AND.equals(jemmaExecution.takeAWord().toUpperCase())) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr5 = jemmaExecution.analyzeExpression(",");
				if (! expr5.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr5.error);
					break;
				}
				if (',' != jemmaExecution.takeAChar()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
					break;
				}
				expr6 = jemmaExecution.analyzeExpression("");
				if (! expr6.valid) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, expr6.error);
					break;
				}
				if (jemmaExecution.notEndOfLine()) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_END_OF_INST);
					break;
				}
				if (execution) {
					newdraw = new myshape();
					newdraw.setTriangle(expr1.result, expr2.result, expr3.result, expr4.result, expr5.result, expr6.result);
					drawing.add(newdraw);
					GC gc = new GC(jemmaUI.ExecutionArea);
					drawTriangle(gc, newdraw, false);
					gc.dispose();
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(expr1.expression).append(",")
							.append(expr2.expression).append(" ")
							.append(jemmaLanguage_Geometry.KEYWORD_AND).append(" ")
							.append(expr3.expression).append(",")
							.append(expr4.expression).append(" ")
							.append(jemmaLanguage_Geometry.KEYWORD_AND).append(" ")
							.append(expr5.expression).append(",")
							.append(expr6.expression).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case 13:        // KEYWORD_XOR
				s = jemmaExecution.takeAWord().toUpperCase();
				if (! s.equals(jemmaLanguage_Geometry.UNIT_YES) && ! s.equals(jemmaLanguage_Geometry.UNIT_NO)) {
					result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_EXPRESSION);
					break;
				}
				if (execution) {
					currentXor = s.equals(jemmaLanguage_Geometry.UNIT_YES);
				} else {
					jemmaExecution.analyzed_program.append(jemmaExecution.addNestedLoopPrefix(jemmaExecution.nestedLoops))
							.append(jemmaLanguage.instructionTab[inst].keywordInstruction).append(" ")
							.append(s).append(jemmaUI.EOL);
				}
				break;
			//--------------------------------------------------------
			case -1:    // Unknown
			default:
				result.correctness = jemmaExecution.displayError(jemmaExecution.lineno, jemmaLanguage.ERROR_UNKNOWN_INST);
				break;
		}
		return result;
	}
	//--------------------------------------------------------------------------
	static void clearDrawing( ) {
		if (drawing == null) return;
		GC gc = new GC(jemmaUI.ExecutionArea);
		for (myshape m : drawing) {
			switch (m.action) {
				case ARC:
					drawArc(gc, m, true);
					break;
				case LINE:
					drawLine(gc, m, true);
					break;
				case OVAL:
					drawOval(gc, m, true);
					break;
				case RECTANGLE:
					drawRectangle(gc, m, true);
					break;
				case ROUNDEDRECTANGLE:
					drawRRectangle(gc, m, true);
					break;
				case TEXT:
					drawText(gc, m, true);
					break;
				case TRIANGLE:
					drawTriangle(gc, m, true);
					break;
			}
		}
		gc.dispose();
		drawing.clear();
	}
	static void Drawing( PaintEvent e ) {
		if (drawing == null) return;
		for (myshape m : drawing) {
			switch (m.action) {
				case ARC:
					drawArc(e.gc, m, false);
					break;
				case LINE:
					drawLine(e.gc, m, false);
					break;
				case OVAL:
					drawOval(e.gc, m, false);
					break;
				case RECTANGLE:
					drawRectangle(e.gc, m, false);
					break;
				case ROUNDEDRECTANGLE:
					drawRRectangle(e.gc, m, false);
					break;
				case TEXT:
					drawText(e.gc, m, false);
					break;
				case TRIANGLE:
					drawTriangle(e.gc, m, false);
					break;
			}
		}
	}
	//--------------------------------------------------------------------------
	private static void drawLine(GC gc, myshape m, boolean erase) {
		if (erase) {
			gc.setForeground(jemmaUI.executionarea_background);
			gc.setXORMode(false);
			gc.setLineStyle(SWT.LINE_SOLID);
			gc.setLineWidth(m.thickness * 2);
		} else {
			gc.setForeground(m.foreground);
			gc.setXORMode(m.xor);
			gc.setLineStyle(m.lineStyle);
			gc.setLineWidth(m.thickness);
		}
		gc.drawLine(m.start.x, m.start.y, m.end.x, m.end.y);
	}
	private static void drawRectangle(GC gc, myshape m, boolean erase) {
		if (erase) {
			gc.setForeground(jemmaUI.executionarea_background);
			gc.setBackground(jemmaUI.executionarea_background);
			gc.setXORMode(false);
			gc.setLineStyle(SWT.LINE_SOLID);
			gc.setLineWidth(m.thickness * 2);
		} else {
			gc.setForeground(m.foreground);
			gc.setBackground(m.background);
			gc.setXORMode(m.xor);
			gc.setLineWidth(m.thickness);
			gc.setLineStyle(m.lineStyle);
		}
		if (m.fill)
			gc.fillRectangle(m.start.x, m.start.y, m.width, m.height);
		else
			gc.drawRectangle(m.start.x, m.start.y, m.width, m.height);
	}
	private static void drawRRectangle(GC gc, myshape m, boolean erase) {
		if (erase) {
			gc.setForeground(jemmaUI.executionarea_background);
			gc.setBackground(jemmaUI.executionarea_background);
			gc.setXORMode(false);
			gc.setLineStyle(SWT.LINE_SOLID);
			gc.setLineWidth(m.thickness * 2);
		} else {
			gc.setForeground(m.foreground);
			gc.setBackground(m.background);
			gc.setXORMode(m.xor);
			gc.setLineWidth(m.thickness);
			gc.setLineStyle(m.lineStyle);
		}
		if (m.fill)
			gc.fillRoundRectangle(m.start.x, m.start.y, m.width, m.height, m.arcWidth, m.arcHeight);
		else
			gc.drawRoundRectangle(m.start.x, m.start.y, m.width, m.height, m.arcWidth, m.arcHeight);
	}
	private static void drawOval(GC gc, myshape m, boolean erase) {
		if (erase) {
			gc.setForeground(jemmaUI.executionarea_background);
			gc.setBackground(jemmaUI.executionarea_background);
			gc.setXORMode(false);
			gc.setLineStyle(SWT.LINE_SOLID);
			gc.setLineWidth(m.thickness * 2);
		} else {
			gc.setForeground(m.foreground);
			gc.setBackground(m.background);
			gc.setXORMode(m.xor);
			gc.setLineWidth(m.thickness);
			gc.setLineStyle(m.lineStyle);
		}
		if (m.fill)
			gc.fillOval(m.start.x, m.start.y, m.width, m.height);
		else
			gc.drawOval(m.start.x, m.start.y, m.width, m.height);
	}
	private static void drawArc(GC gc, myshape m, boolean erase) {
		if (erase) {
			gc.setForeground(jemmaUI.executionarea_background);
			gc.setBackground(jemmaUI.executionarea_background);
			gc.setXORMode(false);
			gc.setLineStyle(SWT.LINE_SOLID);
			gc.setLineWidth(m.thickness * 2);
		} else {
			gc.setForeground(m.foreground);
			gc.setBackground(m.background);
			gc.setXORMode(m.xor);
			gc.setLineWidth(m.thickness);
			gc.setLineStyle(m.lineStyle);
		}
		if (m.fill)
			gc.fillArc(m.start.x, m.start.y, m.width, m.height, m.startAngle, m.endAngle);
		else
			gc.drawArc(m.start.x, m.start.y, m.width, m.height, m.startAngle, m.endAngle);
	}
	private static void drawText(GC gc, myshape m, boolean erase) {
		if (erase) {
			gc.setForeground(jemmaUI.executionarea_background);
			gc.setXORMode(false);
			gc.setLineStyle(SWT.LINE_SOLID);
			gc.setLineWidth(m.thickness * 2);
		} else {
			gc.setForeground(m.foreground);
			gc.setXORMode(m.xor);
			gc.setLineWidth(m.thickness);
			gc.setLineStyle(m.lineStyle);
		}
		gc.drawString(m.text, m.start.x, m.start.y);
	}
	private static void drawTriangle(GC gc, myshape m, boolean erase) {
		if (erase) {
			gc.setForeground(jemmaUI.executionarea_background);
			gc.setBackground(jemmaUI.executionarea_background);
			gc.setXORMode(false);
			gc.setLineStyle(SWT.LINE_SOLID);
			gc.setLineWidth(m.thickness * 2);
		} else {
			gc.setForeground(m.foreground);
			gc.setBackground(m.background);
			gc.setXORMode(m.xor);
			gc.setLineWidth(m.thickness);
			gc.setLineStyle(m.lineStyle);
		}
		if (m.fill)
			gc.fillPolygon(new int[] {m.start.x, m.start.y, m.intermediate.x, m.intermediate.y, m.end.x, m.end.y});
		else
			gc.drawPolygon(new int[] {m.start.x, m.start.y, m.intermediate.x, m.intermediate.y, m.end.x, m.end.y});
	}
}
