#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	Data file processing for C/Java projects  for LINUX and WINDOWS
#
# INPUTS
# ------
# 	ENVIRONMENT VARIABLES:
#		BIN_DIR			Pathname of released executables
#		LIB_DIR			Pathname of released libraries
#		INC_DIR			Pathname of released header files
#
# 	MAKE VARIABLES:
#		MUTE			Prevents or not the command line from echoing out to the console
#		PLATFORM		Host system (WINDOWS, other = LINUX)
#		BINJAR 			Relative path to directory where are stored jar files
#		JARLIBS			List of jar files
#------------------------------------------------------------------------------
ifeq ($(PLATFORM),WINDOWS)
	OBJDIR	= ../windows
else
	OBJDIR	= ../linux
endif
#-------------------------------------------------------------------------------
MKDIR		= mkdir -p
INSTALL		= install -p -v -D
RM			= rm -f
RMDIR		= rm -fr
#-------------------------------------------------------------------------------
install winstall:
	$(MUTE)$(MKDIR) $(BIN_DIR)/$(BINJAR)
	@echo "-- Copying JAR libraries to $(BIN_DIR)/$(BINJAR)"
	$(MUTE)$(INSTALL) -m 644 $(JARLIBS) $(BIN_DIR)/$(BINJAR)
cleaninstall wcleaninstall:
	@echo "-- Removing $(BIN_DIR)/$(BINJAR) jar libraries"
	$(MUTE)$(RM) $(addprefix $(BIN_DIR)/$(BINJAR)/,$(JARLIBS))
#-------------------------------------------------------------------------------
.PHONY: install cleaninstall winstall wcleaninstall
#-------------------------------------------------------------------------------